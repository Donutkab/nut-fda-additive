﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class FunctionalDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        // View All Functional Details
        public IEnumerable<Functional> GetAllFunctionals()
        {
            try
            {
                List<Functional> lstfunctional = new List<Functional>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllFunctionals", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Functional functional = new Functional();

                        functional.functional_id = Convert.ToInt32(rdr["functional_id"]);
                        functional.functional_description = rdr["functional_description"].ToString();

                        lstfunctional.Add(functional);
                    }
                    conn.Close();
                }
                return lstfunctional;
            }
            catch
            {
                throw;
            }
        }

        public Functional GetFunctionalData(int id)
        {
            try
            {
                Functional functional = new Functional();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM functionals WHERE functional_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        functional.functional_id = Convert.ToInt32(rdr["functional_id"]);
                        functional.functional_description = rdr["functional_description"].ToString();
                    }
                }
                return functional;
            }
            catch
            {
                throw;
            }
        }

        public int AddFunctional(Functional functional)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddFunctional", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@functional_description", functional.functional_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public int UpdateFunctional(Functional functional, int functional_id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spUpdateFunctional", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@functional_id", functional_id);
                    cmd.Parameters.AddWithValue("@functional_description", functional.functional_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
