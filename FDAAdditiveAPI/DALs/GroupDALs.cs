﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class GroupDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        public IEnumerable<Group> GetAllGroups()
        {
            try
            {
                List<Group> lstgroup = new List<Group>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllGroups", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Group group = new Group();

                        group.groups_id = Convert.ToInt32(rdr["groups_id"]);
                        group.groups_description = rdr["groups_description"].ToString();

                        lstgroup.Add(group);
                    }
                    con.Close();
                }
                return lstgroup;
            }
            catch
            {
                throw;
            }
        }
        public Group GetGroupsData(int id)
        {
            try
            {
                Group group = new Group();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM groups WHERE groups_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        group.groups_id = Convert.ToInt32(rdr["groups_id"]);
                        group.groups_description = rdr["groups_description"].ToString();
                    }
                }
                return group;
            }
            catch
            {
                throw;
            }
        }

        public int AddGroup(Group group)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddGroup", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@groups_description", group.groups_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public int UpdateGroup(Group group, int group_id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spUpdateGroup", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@groups_id", group_id);
                    cmd.Parameters.AddWithValue("@groups_description", group.groups_description );
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}
