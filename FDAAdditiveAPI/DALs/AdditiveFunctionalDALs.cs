﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class AdditiveFunctionalDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        public IEnumerable<AdditiveFunctional> GetAdditiveFunctionals()
        {
            try
            {
                List<AdditiveFunctional> additiveFunctionals = new List<AdditiveFunctional>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllAdditiveFunctionals", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFunctional additivefunctional = new AdditiveFunctional();

                        additivefunctional.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefunctional.ins_id = rdr["ins_id"].ToString();
                        additivefunctional.additives_description = rdr["additives_description"].ToString();
                        additivefunctional.functional_description = rdr["functional_description"].ToString();

                        additiveFunctionals.Add(additivefunctional);
                    }
                    conn.Close();
                }
                return additiveFunctionals;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveFunctional> GetAdditiveFunctionalsByAdditiveID(int id)
        {
            try
            {
                List<AdditiveFunctional> additiveFunctionals = new List<AdditiveFunctional>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id,additives.additives_description, functionals.functional_description FROM additive_functional INNER JOIN additives ON additive_functional.additives_id = additives.additives_id INNER JOIN functionals ON additive_functional.functional_id = functionals.functional_id WHERE additives.additives_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFunctional additivefunctional = new AdditiveFunctional();
                        additivefunctional.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefunctional.ins_id = rdr["ins_id"].ToString();
                        additivefunctional.additives_description = rdr["additives_description"].ToString();
                        additivefunctional.functional_description = rdr["functional_description"].ToString();
                        additiveFunctionals.Add(additivefunctional);
                    }
                }
                return additiveFunctionals;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveFunctional> GetAdditiveFunctionalsByFunctionalID(int id)
        {
            try
            {
                List<AdditiveFunctional> additiveFunctionals = new List<AdditiveFunctional>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id,additives.additives_description, functionals.functional_description FROM additive_functional INNER JOIN additives ON additive_functional.additives_id = additives.additives_id INNER JOIN functionals ON additive_functional.functional_id = functionals.functional_id WHERE functionals.functional_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFunctional additivefunctional = new AdditiveFunctional();
                        additivefunctional.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefunctional.ins_id = rdr["ins_id"].ToString();
                        additivefunctional.additives_description = rdr["additives_description"].ToString();
                        additivefunctional.functional_description = rdr["functional_description"].ToString();
                        additiveFunctionals.Add(additivefunctional);
                    }
                }
                return additiveFunctionals;
            }
            catch
            {
                throw;
            }
        }



        public int AdditiveFunctionalByAdditiveId(AdditiveFunctional additiveFunctional)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddAdditiveFunctional", conn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    //cmd.Parameters.AddWithValue("@foods_id", food.foods_id);
                    cmd.Parameters.AddWithValue("@additives_id", additiveFunctional.additives_id);
                    cmd.Parameters.AddWithValue("@functional_id", additiveFunctional.functional_id);
           

                    //if (food.parent_id == 0 && food.parent_id == 0 && food.parent_id_1 == 0 && food.parent_id_2 == 0 && food.parent_id_3 == 0)
                    //{
                    //    cmd.Parameters.AddWithValue("@parent_id", 0);
                    //}
                     

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}
