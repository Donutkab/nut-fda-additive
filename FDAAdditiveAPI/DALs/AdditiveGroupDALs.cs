﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class AdditiveGroupDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";
        public IEnumerable<AdditiveGroup> GetAllAddictiveGroups()
        {
            try
            {
                List<AdditiveGroup> additiveGroups = new List<AdditiveGroup>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllAdditiveGroups", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveGroup additivegroup = new AdditiveGroup();

                        additivegroup.groups_description = rdr["groups_description"].ToString();
                        additivegroup.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivegroup.ins_id = rdr["ins_id"].ToString();
                        additivegroup.additives_description = rdr["additives_description"].ToString();
                        additiveGroups.Add(additivegroup);
                    }
                    conn.Close();
                }
                return additiveGroups;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveGroup> GetAdditiveGroupByGroupID(int id)
        {
            try
            {
                List<AdditiveGroup> additiveGroups = new List<AdditiveGroup>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT groups.groups_description, additive_groups.additives_id, additives.ins_id, additives.additives_description FROM additive_groups INNER JOIN groups ON additive_groups.groups_id = groups.groups_id INNER JOIN additives ON additive_groups.additives_id = additives.additives_id WHERE groups.groups_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveGroup additivegroup = new AdditiveGroup();
                        additivegroup.groups_description = rdr["groups_description"].ToString();
                        additivegroup.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivegroup.ins_id = rdr["ins_id"].ToString();
                        additivegroup.additives_description = rdr["additives_description"].ToString();
                        additiveGroups.Add(additivegroup);
                    }
                }
                return additiveGroups;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveGroup> GetAdditiveGroupByAdditiveID(int id)
        {
            try
            {
                List<AdditiveGroup> additiveGroups = new List<AdditiveGroup>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT groups.groups_description, additive_groups.additives_id, additives.ins_id, additives.additives_description FROM additive_groups INNER JOIN groups ON additive_groups.groups_id = groups.groups_id INNER JOIN additives ON additive_groups.additives_id = additives.additives_id WHERE additives.additives_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveGroup additivegroup = new AdditiveGroup();
                        additivegroup.groups_description = rdr["groups_description"].ToString();
                        additivegroup.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivegroup.ins_id = rdr["ins_id"].ToString();
                        additivegroup.additives_description = rdr["additives_description"].ToString();
                        additiveGroups.Add(additivegroup);
                    }
                }
                return additiveGroups;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveGroup> GetAdditiveGroupByGroupDescription(string groups_description)
        {
            try
            {
                List<AdditiveGroup> additiveGroups = new List<AdditiveGroup>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT groups.groups_description, additive_groups.additives_id, additives.ins_id, additives.additives_description FROM additive_groups INNER JOIN groups ON additive_groups.groups_id = groups.groups_id INNER JOIN additives ON additive_groups.additives_id = additives.additives_id WHERE (groups.groups_description like '%" + groups_description + "%') ";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveGroup additivegroup = new AdditiveGroup();
                        additivegroup.groups_description = rdr["groups_description"].ToString();
                        additivegroup.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivegroup.ins_id = rdr["ins_id"].ToString();
                        additivegroup.additives_description = rdr["additives_description"].ToString();
                        additiveGroups.Add(additivegroup);
                    }
                }
                return additiveGroups;
            }
            catch
            {
                throw;
            }
        }

         public IEnumerable<AdditiveGroup> GetDistinctAdditiveGroupByGroupDescription(string groups_description)
        {
            try
            {
                List<AdditiveGroup> additiveGroups = new List<AdditiveGroup>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT DISTINCT(groups.groups_description),groups.groups_id FROM groups WHERE (groups.groups_description like '%" + groups_description + "%') ";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveGroup additivegroup = new AdditiveGroup();
                        additivegroup.groups_description = rdr["groups_description"].ToString();
                        additiveGroups.Add(additivegroup);
                    }
                }
                return additiveGroups;
            }
            catch
            {
                throw;
            }
        }

        
    }
}
