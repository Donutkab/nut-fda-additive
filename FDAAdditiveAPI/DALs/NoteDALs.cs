﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class NoteDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        public IEnumerable<Note> GetAllNotes()
        {
            try
            {
                List<Note> lstnote = new List<Note>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllNotes", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Note note = new Note();

                        note.notes_id = Convert.ToInt32(rdr["notes_id"]);
                        note.notes_code = rdr["notes_code"].ToString();
                        note.notes_description = rdr["notes_description"].ToString();

                        lstnote.Add(note);
                    }
                    conn.Close();
                }
                return lstnote;
            }
            catch
            {
                throw;
            }
        }

        public Note GetNoteData(int id)
        {
            try
            {
                Note note = new Note();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM notes WHERE notes_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        note.notes_id = Convert.ToInt32(rdr["notes_id"]);
                        note.notes_code = rdr["notes_code"].ToString();
                        note.notes_description = rdr["notes_description"].ToString();
                    }
                }
                return note;
            }
            catch
            {
                throw;
            }
        }

        public int AddNote(Note note)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddNote", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@notes_code", note.notes_code);
                    cmd.Parameters.AddWithValue("@notes_description", note.notes_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

        public int UpdateNote(Note note, int note_id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spUpdateNote", con); 
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@notes_id", note_id);
                    cmd.Parameters.AddWithValue("@notes_code", note.notes_code);
                    cmd.Parameters.AddWithValue("@notes_description", note.notes_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
