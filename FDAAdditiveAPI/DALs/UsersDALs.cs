﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class UsersDALs

    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";


        public Users CheckLogin(Users users)
        {
            try
            {

                Users note = new Users();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spChkLogin", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@username", users.username);
                    cmd.Parameters.AddWithValue("@password", users.password);

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        note.count = Convert.ToInt32(rdr["count"]);
                        note.fullname = rdr["fullname"].ToString();


                    }
                    conn.Close();
                }
                return note;
            }
            catch
            {
                throw;
            }
        }


        //public Users CheckLogin(Users users)
        //{
        //    try
        //    {

        //        Users note = new Users();
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            SqlCommand cmd = new SqlCommand("spLoginCheck", conn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.AddWithValue("@userName", users.userName);
        //            cmd.Parameters.AddWithValue("@password", users.password);

        //            conn.Open();

        //            SqlDataReader rdr = cmd.ExecuteReader();

        //            while (rdr.Read())
        //            {
        //                note.count = Convert.ToInt32(rdr["count"]);
        //                note.Name = rdr["name"].ToString();


        //            }
        //            conn.Close();
        //        }
        //        return note;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}




        //public Users CheckLogin(Users users)
        //{
        //    try
        //    {
        //        //Users note = new Users();
        //        List<Users> note = new List<Users>();


        //        using (SqlConnection con = new SqlConnection(connectionString))
        //        {
        //            //string sqlQuery = $"SELECT * FROM [fdaadditive].[dbo].[users] WHERE ([fdaadditive].[dbo].[users].[userName]='" + users.userName + "') AND ([fdaadditive].[dbo].[users].password='" + users.password +"') ";

        //            //string sqlQuery = $"SELECT * FROM [fdaadditive].[dbo].[users] WHERE ([fdaadditive].[dbo].[users].[userName]='" + users.userName + "') AND ([fdaadditive].[dbo].[users].password='" + users.password + "') ";


        //            string sqlQuery = "SELECT COUNT(*) as count FROM users WHERE (users.userName='" + users.userName + "') AND (users.password='" + users.password + "') ";


        //            //SELECT* FROM users WHERE userName = @userName AND password = @password

        //            SqlCommand cmd = new SqlCommand(sqlQuery, con);

        //            con.Open();
        //            SqlDataReader rdr = cmd.ExecuteReader();

        //            while (rdr.Read())
        //            {
        //                Users userdata = new Users();

        //                note.count = Convert.ToInt32(rdr["count"]);
        //                note.Add(userdata);

        //            }
        //        }
        //        return note;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}


    }
}
