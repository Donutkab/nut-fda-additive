﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class AdditiveFoodDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        public IEnumerable<AdditiveFood> GetAdditiveFoods()
        {
            try
            {
                List<AdditiveFood> additiveFoods = new List<AdditiveFood>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllAdditiveFoods", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFood additivefood = new AdditiveFood();

                        additivefood.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefood.ins_id = rdr["ins_id"].ToString();
                        additivefood.addictives_description = rdr["additives_description"].ToString();
                        additivefood.foods_name = rdr["foods_name"].ToString();
                        additivefood.foods_code = rdr["foods_code"].ToString();
                        additivefood.foods_description_th = rdr["foods_description_th"].ToString();
                        additivefood.foods_description_en = rdr["foods_description_en"].ToString();
                        additivefood.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        additivefood.max_level = rdr["max_level"].ToString();
                        additivefood.years = rdr["years"].ToString();

                        additiveFoods.Add(additivefood);
                    }
                    conn.Close();
                }
                return additiveFoods;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveFood> GetAdditiveFoodByAdditiveID(int id)
        {
            try
            {
                List<AdditiveFood> additiveFoods = new List<AdditiveFood>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, foods.foods_code, foods.foods_name, foods.foods_description_th, foods.foods_description_en, foods.parent_id, additive_foods.max_level, additive_foods.years FROM additive_foods INNER JOIN foods ON additive_foods.foods_id = foods.foods_id INNER JOIN additives ON additive_foods.additives_id = additives.additives_id WHERE additives.additives_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFood additivefood = new AdditiveFood();
                        additivefood.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefood.ins_id = rdr["ins_id"].ToString();
                        additivefood.addictives_description = rdr["additives_description"].ToString();
                        additivefood.foods_code = rdr["foods_code"].ToString();
                        additivefood.foods_name = rdr["foods_name"].ToString();
                        additivefood.foods_description_th = rdr["foods_description_th"].ToString();
                        additivefood.foods_description_en = rdr["foods_description_en"].ToString();
                        additivefood.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        additivefood.max_level = rdr["max_level"].ToString();
                        additivefood.years = rdr["years"].ToString();
                        additiveFoods.Add(additivefood);
                    }
                }
                return additiveFoods;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveFood> GetAdditiveFoodByFoodCode(string foods_code)
        {
            try
            {
                List<AdditiveFood> additiveFoods = new List<AdditiveFood>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, foods.foods_code, foods.foods_name, foods.foods_description_th, foods.foods_description_en, foods.parent_id, additive_foods.max_level, additive_foods.years FROM additive_foods INNER JOIN foods ON additive_foods.foods_id = foods.foods_id INNER JOIN additives ON additive_foods.additives_id = additives.additives_id WHERE (foods.foods_code='" + foods_code + "') OR (foods.foods_name like '%" + foods_code + "%') ";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFood additivefood = new AdditiveFood();
                        additivefood.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefood.ins_id = rdr["ins_id"].ToString();
                        additivefood.addictives_description = rdr["additives_description"].ToString();
                        additivefood.foods_code = rdr["foods_code"].ToString();
                        additivefood.foods_name = rdr["foods_name"].ToString();
                        additivefood.foods_description_th = rdr["foods_description_th"].ToString();
                        additivefood.foods_description_en = rdr["foods_description_en"].ToString();
                        additivefood.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        additivefood.max_level = rdr["max_level"].ToString();
                        additivefood.years = rdr["years"].ToString();
                        additiveFoods.Add(additivefood);
                    }
                }
                return additiveFoods;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveFood> GetAdditiveFoodByFoodID(int id)
        {
            try
            {
                List<AdditiveFood> additiveFoods = new List<AdditiveFood>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, foods.foods_code, foods.foods_name, foods.foods_description_th, foods.foods_description_en, foods.parent_id, additive_foods.max_level, additive_foods.years FROM additive_foods INNER JOIN foods ON additive_foods.foods_id = foods.foods_id INNER JOIN additives ON additive_foods.additives_id = additives.additives_id WHERE additive_foods.foods_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveFood additivefood = new AdditiveFood();
                        additivefood.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivefood.ins_id = rdr["ins_id"].ToString();
                        additivefood.addictives_description = rdr["additives_description"].ToString();
                        additivefood.foods_code = rdr["foods_code"].ToString();
                        additivefood.foods_name = rdr["foods_name"].ToString();
                        additivefood.foods_description_th = rdr["foods_description_th"].ToString();
                        additivefood.foods_description_en = rdr["foods_description_en"].ToString();
                        additivefood.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        additivefood.max_level = rdr["max_level"].ToString();
                        additivefood.years = rdr["years"].ToString();
                        additiveFoods.Add(additivefood);
                    }
                }
                return additiveFoods;
            }
            catch
            {
                throw;
            }
        }

        public int AddAdditiveFood(AdditiveFood additiveFood)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddAdditiveFoods", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@additives_id", additiveFood.additives_id);
                    cmd.Parameters.AddWithValue("@foods_id", additiveFood.foods_id);
                    cmd.Parameters.AddWithValue("@max_level", additiveFood.max_level);
                    cmd.Parameters.AddWithValue("@notes", additiveFood.notes);
                    cmd.Parameters.AddWithValue("@years", additiveFood.years);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
