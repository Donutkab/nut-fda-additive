﻿using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class AdditiveDALs
    {
        string connectionString = "Data Source=(local); Initial Catalog=fdaadditive; Integrated Security=True";
        public IEnumerable<Additive> GetAdditives()
        {
            try
            {

                List<Additive> additives = new List<Additive>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllAdditives", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Additive additive = new Additive();

                        additive.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additive.ins_id = rdr["ins_id"].ToString();
                        additive.additives_description = rdr["additives_description"].ToString();

                        additives.Add(additive);
                    }
                    conn.Close();
                }
                return additives;
            }
            catch
            {
                throw;
            }
        }

        public Additive GetAdditiveByID(int id)
        {
            try
            {
                Additive additive = new Additive();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM additives WHERE additives_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        additive.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additive.ins_id = rdr["ins_id"].ToString();
                        additive.additives_description = rdr["additives_description"].ToString();
                    }
                }
                return additive;
            }
            catch
            {
                throw;
            }
        }
        public Additive GetAdditiveByINS(string ins)
        {
            try
            {
                Additive additive = new Additive();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM additives WHERE (ins_id='" + ins + "')";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.HasRows == false)
                    {
                        return null;
                    }
                    else
                    {
                        while (rdr.Read())
                        {
                            additive.additives_id = Convert.ToInt32(rdr["additives_id"]);
                            additive.ins_id = rdr["ins_id"].ToString();
                            additive.additives_description = rdr["additives_description"].ToString();
                        }
                    }

                }
                return additive;
            }
            catch
            {
                throw;
            }
        }
        public IEnumerable<Additive> GetAdditiveByDescription(string additives_description)
        {
            try
            {
                List<Additive> additives = new List<Additive>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM additives WHERE (additives_description like '%" + additives_description + "%')";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            Additive additive = new Additive();

                            additive.additives_id = Convert.ToInt32(rdr["additives_id"]);
                            additive.ins_id = rdr["ins_id"].ToString();
                            additive.additives_description = rdr["additives_description"].ToString();

                            additives.Add(additive);
                        }
                    }
                    else
                    {

                    }
                }
                return additives;
            }
            catch
            {
                throw;
            }
        }

        public int AddAdditive(Additive additive)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddAdditives", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@ins_id", additive.ins_id);
                    cmd.Parameters.AddWithValue("@additives_description", additive.additives_description);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }


        public int UpdateAdditive(int id, Additive additive)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spUpdateAdditives", conn);  
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.AddWithValue("@additives_id", id);
                    cmd.Parameters.AddWithValue("@ins_id", additive.ins_id);
                    cmd.Parameters.AddWithValue("@additives_description", additive.additives_description);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
