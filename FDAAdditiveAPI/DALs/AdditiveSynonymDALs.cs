﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class AdditiveSynonymDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";
        public IEnumerable<AdditiveSynonym> GetAllAdditiveSynonyms()
        {
            try
            {
                List<AdditiveSynonym> additiveSynonyms = new List<AdditiveSynonym>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllAdditiveSynonyms", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveSynonym additiveSynonym = new AdditiveSynonym();

                        additiveSynonym.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additiveSynonym.ins_id = rdr["ins_id"].ToString();
                        additiveSynonym.additives_description = rdr["additives_description"].ToString();
                        additiveSynonym.additives_synonyms = rdr["additives_synonyms"].ToString();

                        additiveSynonyms.Add(additiveSynonym);
                    }
                    conn.Close();
                }
                return additiveSynonyms;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveSynonym> GetAdditiveSynonymByAdditivesID(int id)
        {
            try
            {
                List<AdditiveSynonym> additiveSynonyms = new List<AdditiveSynonym>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, additive_synonym.additives_synonyms FROM additive_synonym INNER JOIN additives ON additive_synonym.additives_id = additives.additives_id WHERE additives.additives_id= " + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        AdditiveSynonym additivesynonym = new AdditiveSynonym();

                        additivesynonym.additives_id = Convert.ToInt32(rdr["additives_id"]);
                        additivesynonym.ins_id = rdr["ins_id"].ToString();
                        additivesynonym.additives_description = rdr["additives_description"].ToString();
                        additivesynonym.additives_synonyms = rdr["additives_synonyms"].ToString();

                        additiveSynonyms.Add(additivesynonym);
                    }
                }
                return additiveSynonyms;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveSynonym> GetAdditiveSynonymByAdditiveSynonyms(string additives_synonyms)
        {
            try
            {
                List<AdditiveSynonym> additiveSynonyms = new List<AdditiveSynonym>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, additive_synonym.additives_synonyms FROM additive_synonym INNER JOIN additives ON additive_synonym.additives_id = additives.additives_id WHERE (additives_synonyms like '%" + additives_synonyms + "%') ORDER BY additives.additives_id";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.HasRows == false)
                    {
                        return null;
                    }
                    else
                    {
                        while (rdr.Read())
                        {
                            AdditiveSynonym additivesynonym = new AdditiveSynonym();

                            additivesynonym.additives_id = Convert.ToInt32(rdr["additives_id"]);
                            additivesynonym.ins_id = rdr["ins_id"].ToString();
                            additivesynonym.additives_description = rdr["additives_description"].ToString();
                            additivesynonym.additives_synonyms = rdr["additives_synonyms"].ToString();

                            additiveSynonyms.Add(additivesynonym);
                        }
                    }
                }
                return additiveSynonyms;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<AdditiveSynonym> GetAdditiveSynonymByAdditiveDescription(string additives_description)
        {
            try
            {
                List<AdditiveSynonym> additiveSynonyms = new List<AdditiveSynonym>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT additives.additives_id, additives.ins_id, additives.additives_description, additive_synonym.additives_synonyms FROM additive_synonym INNER JOIN additives ON additive_synonym.additives_id = additives.additives_id WHERE (additives_description like '%" + additives_description + "%')";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.HasRows == false)
                    {
                        return null;
                    }
                    else
                    {
                        while (rdr.Read())
                        {
                            AdditiveSynonym additivesynonym = new AdditiveSynonym();

                            additivesynonym.additives_id = Convert.ToInt32(rdr["additives_id"]);
                            additivesynonym.ins_id = rdr["ins_id"].ToString();
                            additivesynonym.additives_description = rdr["additives_description"].ToString();
                            additivesynonym.additives_synonyms = rdr["additives_synonyms"].ToString();

                            additiveSynonyms.Add(additivesynonym);
                        }
                    }
                }
                return additiveSynonyms;
            }
            catch
            {
                throw;
            }
        }



        public int AddSynonym(AdditiveSynonym additiveSynonym)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddAdditiveSynonym", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@additives_id", additiveSynonym.additives_id);
                    cmd.Parameters.AddWithValue("@additives_synonyms", additiveSynonym.additives_synonyms);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }


    }
}
