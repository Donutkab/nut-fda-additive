﻿using FDAAdditiveAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.DALs
{
    public class FoodDALs
    {
        string connectionString = "Data Source=(local);Initial Catalog=fdaadditive;Integrated Security=True";

        public IEnumerable<Food> GetAllFoods()
        {
            try
            {
                List<Food> lstfood = new List<Food>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllFoods", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Food food = new Food();

                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();

                        lstfood.Add(food);
                    }
                    conn.Close();
                }
                return lstfood;
            }
            catch
            {
                throw;
            }
        }

        public Food GetFoodDataByFoodID(int id)
        {
            try
            {
                Food food = new Food();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM foods WHERE foods_id=" + id;
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();
                    }
                }
                return food;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Food> GetFoodDataByFoodCode(string foods_code)
        {
            try
            {
                List<Food> lstfood = new List<Food>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM foods WHERE (foods_code like '%" + foods_code + "%') OR (foods_name like '%" + foods_code + "%')";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Food food = new Food();
                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();

                        lstfood.Add(food);
                    }
                }
                return lstfood;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Food> GetFoodDataByFoodName(string foods_name)
        {
            try
            {
                List<Food> lstfood = new List<Food>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM foods WHERE (foods_name like '%" + foods_name + "%')";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Food food = new Food();
                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();

                        lstfood.Add(food);
                    }
                }
                return lstfood;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Food> GetFoodDataForList()
        {
            try
            {
                List<Food> lstfood = new List<Food>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM foods WHERE parent_id='0'";
                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Food food = new Food();
                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();
                        food.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        lstfood.Add(food);
                    }
                }
                return lstfood;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Food> GetFoodDataByParentId(int foods_id)
        {
            try
            {
                List<Food> lstfood = new List<Food>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM foods WHERE parent_id=" + foods_id;

                    SqlCommand cmd = new SqlCommand(sqlQuery, con);

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Food food = new Food();
                        food.foods_id = Convert.ToInt32(rdr["foods_id"]);
                        food.foods_code = rdr["foods_code"].ToString();
                        food.foods_name = rdr["foods_name"].ToString();
                        food.foods_description_th = rdr["foods_description_th"].ToString();
                        food.foods_description_en = rdr["foods_description_en"].ToString();
                        food.parent_id = Convert.ToInt32(rdr["parent_id"]);
                        lstfood.Add(food);
                    }
                }
                return lstfood;
            }
            catch
            {
                throw;
            }
        }


        public int UpdateFood(int id,Food food)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spUpdateFood", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@foods_id", id);
                    cmd.Parameters.AddWithValue("@foods_code", food.foods_code);
                    cmd.Parameters.AddWithValue("@foods_name", food.foods_name);
                    cmd.Parameters.AddWithValue("@foods_description_th", food.foods_description_th);
                    cmd.Parameters.AddWithValue("@foods_description_en", food.foods_description_en);
                    cmd.Parameters.AddWithValue("@parent_id", food.parent_id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }




        public int AddFood(Food food)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spAddFood", conn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    //cmd.Parameters.AddWithValue("@foods_id", food.foods_id);
                    cmd.Parameters.AddWithValue("@foods_code", food.foods_code);
                    cmd.Parameters.AddWithValue("@foods_name", food.foods_name);
                    cmd.Parameters.AddWithValue("@foods_description_th", food.foods_description_th);
                    cmd.Parameters.AddWithValue("@foods_description_en", food.foods_description_en);

                    if (food.parent_id == 0 && food.parent_id == 0 && food.parent_id_1 == 0 && food.parent_id_2 == 0 && food.parent_id_3 == 0)
                    {
                        cmd.Parameters.AddWithValue("@parent_id", 0);
                    }
                    else if (food.parent_id != 0 && food.parent_id_1 == 0 && food.parent_id_2 == 0 && food.parent_id_3 == 0) 
                    {
                        cmd.Parameters.AddWithValue("@parent_id", food.parent_id);
                    }

                    else if (food.parent_id != 0 && food.parent_id_1 !=0 && food.parent_id_2 == 0 && food.parent_id_3 == 0)
                    {
                        cmd.Parameters.AddWithValue("@parent_id", food.parent_id_1);
                    }
                    
                    else if (food.parent_id != 0 && food.parent_id_1 != 0 && food.parent_id_2 != 0 && food.parent_id_3 == 0)
                    {
                        cmd.Parameters.AddWithValue("@parent_id", food.parent_id_2);
                    }

                    else if (food.parent_id != 0 && food.parent_id_1 != 0 && food.parent_id_2 != 0 && food.parent_id_3 != 0)
                    {
                        cmd.Parameters.AddWithValue("@parent_id", food.parent_id_3);
                    }

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //public int AddAdditive(Food food)
        //{
        //    try
        //    {
        //        using (SqlConnection con = new SqlConnection(connectionString))
        //        {
        //            SqlCommand cmd = new SqlCommand("spAddAdditives", con);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.AddWithValue("@foods_name", food.foods_name);
        //            cmd.Parameters.AddWithValue("@foods_description_th", food.foods_description_th);
        //            cmd.Parameters.AddWithValue("@foods_description_en", food.foods_description_en);
        //            cmd.Parameters.AddWithValue("@foods_description_th", food.foods_description_th);
        //            con.Open();
        //            cmd.ExecuteNonQuery();
        //            con.Close();
        //        }
        //        return 1;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}


    }
}
