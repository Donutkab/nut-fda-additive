﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/additivefoods")]
    [ApiController]
    public class AdditiveFoodController : ControllerBase
    {
        AdditiveFoodDALs additiveFoodDALs = new AdditiveFoodDALs();

        /// <summary>
        /// Get the list of all additives with food details
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefoods
        [HttpGet]
        public IEnumerable<AdditiveFood> Get()
        {
            return additiveFoodDALs.GetAdditiveFoods();
        }

        /// <summary>
        /// Get the details of additive with food details by additive id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefoods/5
        [HttpGet("{id}")]
        public IEnumerable<AdditiveFood> Get(int id)
        {
            return additiveFoodDALs.GetAdditiveFoodByAdditiveID(id);
        }

        /// <summary>
        /// Get the details of additive with food details by foods_code or foods_category
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefoods/foodcode/เนย
        [HttpGet("foodcode/{foods_code}")]
        public IEnumerable<AdditiveFood> GetByFoodsCode(string foods_code)
        {
            return additiveFoodDALs.GetAdditiveFoodByFoodCode(foods_code);
        }

        /// <summary>
        /// Get the details of additive with food details by foods_id
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefoods/foodcode/เนย
        [HttpGet("id/{id}")]
        public IEnumerable<AdditiveFood> GetByFoodsID(int id)
        {
            return additiveFoodDALs.GetAdditiveFoodByFoodID(id);
        }

        /// <summary>
        /// Add New Additive Foods Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefoods/create/9999
        [HttpPost]
        [Route("api/v1/additivefoods/create")]
        public int Create([FromBody] AdditiveFood additiveFood)
        {
            return additiveFoodDALs.AddAdditiveFood(additiveFood);
        }
    }
}
