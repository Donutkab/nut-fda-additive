﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/Functional")]
    [ApiController]
    public class FunctionalController : ControllerBase
    {
        FunctionalDALs functionalDALs = new FunctionalDALs();

        /// <summary>
        /// Get the list of all functionals
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/functionals
        [HttpGet]
        public IEnumerable<Functional> Get()
        {
            return functionalDALs.GetAllFunctionals();
        }

        /// <summary>
        /// Get the details of functional by functional id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/functional/5
        [HttpGet("{id}")]
        public Functional Get(int id)
        {
            return functionalDALs.GetFunctionalData(id);
        }

        /// <summary>
        /// Add New Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Functional/create/9999
        [HttpPost]
        [Route("api/v1/Functional/create")]
        public int Create([FromBody] Functional functional)
        {
            return functionalDALs.AddFunctional(functional);
        }

        /// <summary>
        /// Update Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Functional/update/1
        [HttpPut]
        [Route("api/v1/Functional/update")]
        public int Update([FromBody] Functional functional,int functional_id)
        {
            return functionalDALs.UpdateFunctional(functional, functional_id);
        }
    }
}
