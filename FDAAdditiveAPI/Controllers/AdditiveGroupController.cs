﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/additivegroups")]
    [ApiController]
    public class AdditiveGroupController : ControllerBase
    {
        AdditiveGroupDALs additiveGroupDALs = new AdditiveGroupDALs();
        /// <summary>
        /// Get the list of all additives with group details
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivegroups
        [HttpGet]
        public IEnumerable<AdditiveGroup> Get()
        {
            return additiveGroupDALs.GetAllAddictiveGroups();
        }

        /// <summary>
        /// Get the list of all additives with functional details by Group ID
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivegroups/5
        [HttpGet("{id}")]
        public IEnumerable<AdditiveGroup> Get(int id)
        {
            return additiveGroupDALs.GetAdditiveGroupByGroupID(id);
        }

        /// <summary>
        /// Get the list of all additives with functional details by additive ID
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivegroups/additive/5
        [HttpGet("additive/{id}")]
        public IEnumerable<AdditiveGroup> GetAdditiveGroupByAdditiveID(int id)
        {
            return additiveGroupDALs.GetAdditiveGroupByAdditiveID(id);
        }

        /// <summary>
        /// Get the list of all additives with functional details by additive ID
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivegroups/group/5
        [HttpGet("groupcode/{groups_description}")]
        public IEnumerable<AdditiveGroup> GetAdditiveGroupByGroupDescription(string groups_description)
        {
            return additiveGroupDALs.GetAdditiveGroupByGroupDescription(groups_description);
        }

         /// <summary>
        /// Get distinct the list of all additives groups
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivegroups/group/5
        [HttpGet("groupdetail/{groups_description}")]
        public IEnumerable<AdditiveGroup> GetDistinctAdditiveGroupByGroupDescription(string groups_description)
        {
            return additiveGroupDALs.GetDistinctAdditiveGroupByGroupDescription(groups_description);
        }


    }
}
