﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/notes")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        NoteDALs noteDALs = new NoteDALs();

        /// <summary>
        /// Get the list of all notes
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/notes
        [HttpGet]
        public IEnumerable<Note> Get()
        {
            return noteDALs.GetAllNotes();
        }

        /// <summary>
        /// Get the details of notes by notes id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/notes/5
        [HttpGet("{id}")]
        public Note Get(int id)
        {
            return noteDALs.GetNoteData(id);
        }
        /// <summary>
        /// Add New Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Note/create/9999
        [HttpPost]
        [Route("api/v1/Note/create")]
        public int Create([FromBody] Note note)
        {
            return noteDALs.AddNote(note);
        }

        /// <summary>
        /// Update Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Note/update/1
        [HttpPut]
        [Route("api/v1/Note/update")]
        public int Update([FromBody] Note note, int note_id)
        {
            return noteDALs.UpdateNote(note, note_id);
        }
    }
}
