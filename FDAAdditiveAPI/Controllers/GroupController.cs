﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/groups")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        GroupDALs groupDALs = new GroupDALs();

        /// <summary>
        /// Get the list of all groups
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/groups
        [HttpGet]
        public IEnumerable<Group> Get()
        {
            return groupDALs.GetAllGroups();
        }

        /// <summary>
        /// Get the details of group by group id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/groups/5
        [HttpGet("{id}")]
        public Group Get(int id)
        {
            return groupDALs.GetGroupsData(id);
        }
        /// <summary>
        /// Add New Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Group/create/9999
        [HttpPost]
        [Route("api/v1/Group/create")]
        public int Create([FromBody] Group group)
        {
            return groupDALs.AddGroup(group);
        }

        /// <summary>
        /// Update Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Group/update/1
        [HttpPut]
        [Route("api/v1/Group/update")]
        public int Update([FromBody] Group group, int group_id)
        {
            return groupDALs.UpdateGroup(group, group_id);
        }
    }
}
