﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/additives")]
    [ApiController]
    public class AdditiveController : ControllerBase
    {
        AdditiveDALs additiveDALs = new AdditiveDALs();

        /// <summary>
        /// Get the list of all additives
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additives
        [HttpGet]
        public IEnumerable<Additive> Get()
        {
            return additiveDALs.GetAdditives();
        }

        /// <summary>
        /// Get the details of additive by additive id
        /// </summary> 
        /// <param name="id">The Additive ID of the desired Additive</param>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additives/5
        [HttpGet("{id}")]
        public Additive Get(int id)
        {
            return additiveDALs.GetAdditiveByID(id);
        }

        /// <summary>
        /// Get the details of additive by additive ins id
        /// </summary> 
        /// <param name="ins">The Additive INS ID of the desired Additive</param>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additives/ins/950
        [HttpGet("ins/{ins}")]
        public Additive GetByIns(string ins)
        {
            return additiveDALs.GetAdditiveByINS(ins);
        }

        /// <summary>
        /// Get the details of additive by additive ins id
        /// </summary> 
        /// <param name="additives_description">The Additive Description of the desired Additive</param>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additives/description/acid
        [HttpGet("description/{additives_description}")]
        public IEnumerable<Additive> GetByDescription(string additives_description)
        {
            return additiveDALs.GetAdditiveByDescription(additives_description);
        }

        /// <summary>
        /// Create a new additive and return the details of the new additive created
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // POST: api/v1/additive/create
        [HttpPost]
        [Route("api/v1/additive/create")]
        public int Create([FromBody] Additive additive)
        {
            return additiveDALs.AddAdditive(additive);
        }

        /// <summary>
        /// Update the additive or create the additive, if it doesn't exist.
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // PUT: api/v1/additive/5
        //[HttpPut("{id}")]
        //public void Update(int id, [FromBody] Additive additive)
        //{
        //}

        [HttpPut]
        [Route("api/v1/additive/update")]
        public int Update(int id, Additive additive)
        {
            return additiveDALs.UpdateAdditive(id, additive);
        }

    }
}
