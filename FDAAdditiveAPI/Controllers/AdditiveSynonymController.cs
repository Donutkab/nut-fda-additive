﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/additivesynonym")]
    [ApiController]
    public class AdditiveSynonymController : ControllerBase
    {
        AdditiveSynonymDALs additiveSynonymDAL = new AdditiveSynonymDALs();
        /// <summary>
        /// Get the list of all additives with synonyms details
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivesynonym
        [HttpGet]
        public IEnumerable<AdditiveSynonym> Get()
        {
            return additiveSynonymDAL.GetAllAdditiveSynonyms();
        }

        /// <summary>
        /// Get the details of additive by additive id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivesynonym/5
        [HttpGet("{id}")]
        public IEnumerable<AdditiveSynonym> Get(int id)
        {
            return additiveSynonymDAL.GetAdditiveSynonymByAdditivesID(id);
        }

        /// <summary>
        /// Get the details of additive by additive synonym
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivesynonym/details/5
        [HttpGet("details/{additives_synonyms}")]
        public IEnumerable<AdditiveSynonym> GetAdditiveSynonymByAdditiveSynonyms(string additives_synonyms)
        {
            return additiveSynonymDAL.GetAdditiveSynonymByAdditiveSynonyms(additives_synonyms);
        }

        /// <summary>
        /// Get the details of additive by additive description
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivesynonym/detailsbydescription/5
        [HttpGet("detailsbydescription/{additives_description}")]
        public IEnumerable<AdditiveSynonym> GetAdditiveSynonymByAdditiveDescription(string additives_description)
        {
            return additiveSynonymDAL.GetAdditiveSynonymByAdditiveDescription(additives_description);
        }


        /// <summary>
        /// Add New Synonym Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Synonym/create/9999
        [HttpPost]
        [Route("api/v1/Synonym/create")]
        public int Create([FromBody] AdditiveSynonym additiveSynonym)
        {
            return additiveSynonymDAL.AddSynonym(additiveSynonym);
        }

    }
}
