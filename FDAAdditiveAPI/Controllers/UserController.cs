﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        UsersDALs usersDALs = new UsersDALs();
        // GET: api/User
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        //// POST: api/User
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

      

        /// <summary>
        /// Add New Functional Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Note/create/9999
        [HttpPost]
        [Route("api/v1/Users/login")]
        public Users Post([FromBody] Users user)
        {
            return usersDALs.CheckLogin(user);
        }



        // PUT: api/User/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
