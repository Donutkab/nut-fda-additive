﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/Food")]
    [ApiController]
    public class FoodController : ControllerBase
    {
        FoodDALs foodDALs = new FoodDALs();

        /// <summary>
        /// Get the list of all foods
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods
        [HttpGet]
        public IEnumerable<Food> Get()
        {
            return foodDALs.GetAllFoods();
        }

        /// <summary>
        /// Get the details of food by food id
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods/5
        [HttpGet("{id}")]
        public Food Get(int id)
        {
            return foodDALs.GetFoodDataByFoodID(id);
        }

        /// <summary>
        /// Get the details of food by food code
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods/foodscode/5
        [HttpGet("foodscode/{foods_code}")]
        public IEnumerable<Food> GetFoodDataByFoodCode(string foods_code)
        {
            return foodDALs.GetFoodDataByFoodCode(foods_code);
        }

        /// <summary>
        /// Get the details of food by food name
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods/foodscode/5
        [HttpGet("foodsname/{foods_name}")]
        public IEnumerable<Food> GetFoodDataByFoodName(string foods_name)
        {
            return foodDALs.GetFoodDataByFoodName(foods_name);
        }

        /// <summary>
        /// Get the details of food for List
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods/foodslist
        [HttpGet("foods/foodslist")]
        public IEnumerable<Food> GetFoodDataForList()
        {
            return foodDALs.GetFoodDataForList();
        }


        /// <summary>
        /// Get the List of food for Dropdown List
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/foods/dropdownlist
        
        [HttpGet("foods/{foods_id}")]
        //[HttpGet("foods/dropdownlist")]
        public IEnumerable<Food> GetFoodDataForDropdpwnList(int foods_id)
        {
            return foodDALs.GetFoodDataByParentId(foods_id);
        }

        /// <summary>
        /// Create a new food and return the details of the new food created
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // POST: api/v1/food/create
        [HttpPost]
        [Route("api/v1/food/create")]
        public int Create([FromBody] Food food)
        {
            return foodDALs.AddFood(food);
        }



        /// <summary>
        /// Update Food Details
        /// </summary>
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/Food/update/1
        [HttpPut]
        [Route("api/v1/Food/update")]
        public int Update(int id, Food food)
        {
            return foodDALs.UpdateFood(id, food);
        }


    }
}
