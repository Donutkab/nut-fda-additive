﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FDAAdditiveAPI.DALs;
using FDAAdditiveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FDAAdditiveAPI.Controllers
{
    [Route("api/v1/additivefunctionals")]
    [ApiController]
    public class AdditiveFunctionalController : ControllerBase
    {
        AdditiveFunctionalDALs additiveFunctionalDALs = new AdditiveFunctionalDALs();
        /// <summary>
        /// Get the list of all additives with functional details
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefunctionals
        [HttpGet]
        public IEnumerable<AdditiveFunctional> Get()
        {
            return additiveFunctionalDALs.GetAdditiveFunctionals();
        }

        /// <summary>
        /// Get the list of all additives with functional details by Additive ID
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefunctionals/5
        [HttpGet("{id}")]
        public IEnumerable<AdditiveFunctional> Get(int id)
        {
            return additiveFunctionalDALs.GetAdditiveFunctionalsByAdditiveID(id);
        }

        /// <summary>
        /// Get the list of all additives with functional details by Functional ID
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // GET: api/v1/additivefunctionals/details/5
        [HttpGet("details/{id}")]
        public IEnumerable<AdditiveFunctional> GetAdditiveFunctionalsByFunctionalID(int id)
        {
            return additiveFunctionalDALs.GetAdditiveFunctionalsByFunctionalID(id);
        }


        /// <summary>
        /// Create a new additivefunctional and return the details of the new additivefunctional created
        /// </summary> 
        /// <response code="401">Unauthorized Access</response>
        // POST: api/v1/additivefunctional /create
        [HttpPost]
        [Route("api/v1/additivefunctional/create")]
        public int Create([FromBody] AdditiveFunctional additiveFunctional)
        {
            return additiveFunctionalDALs.AdditiveFunctionalByAdditiveId(additiveFunctional);
        }
    }
}
