﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Note
    {
        public int notes_id { get; set; }
        public string notes_code { get; set; }
        public string notes_description { get; set; }

    }
}
