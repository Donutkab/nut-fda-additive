﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Group
    {
        public int groups_id { get; set; }
        public string groups_description { get; set; }

    }
}
