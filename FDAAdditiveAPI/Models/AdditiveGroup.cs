﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class AdditiveGroup
    {
        public int additives_id { get; set; }
        public string ins_id { get; set; }
        public string additives_description { get; set; }
        public string groups_description { get; set; }

    }
}
