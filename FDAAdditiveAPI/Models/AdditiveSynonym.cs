﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class AdditiveSynonym
    {
        public int additives_id { get; set; }
        public string ins_id { get; set; }
        public string additives_description { get; set; }
        public string additives_synonyms { get; set; }

    }
}
