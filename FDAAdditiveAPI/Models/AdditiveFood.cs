﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class AdditiveFood
    {
        public int additives_id { get; set; }
        public string ins_id { get; set; }
        public string addictives_description { get; set; }
        public int foods_id { get; set; }
        public string foods_code { get; set; }
        public string foods_name { get; set; }
        public string foods_description_th { get; set; }
        public string foods_description_en { get; set; }
        public int parent_id { get; set; }
        public string max_level { get; set; }
        public string notes { get; set; }
        public string years { get; set; }

    }
}
