﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Food
    {
        public int foods_id { get; set; }
        public string foods_code { get; set; }
        public string foods_name { get; set; }
        public string foods_description_th { get; set; }
        public string foods_description_en { get; set; }
        public int parent_id { get; set; }
        public int parent_id_1 { get; set; }
        public int parent_id_2 { get; set; }
        public int parent_id_3 { get; set; }



    }
}
