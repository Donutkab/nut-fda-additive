﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Users
    {
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string userName { get; set; }
        //public string password { get; set; }
        //public int count { get; set; }

        public int userID { get; set; }
        public string fullname { get; set; }
        public string username { get; set; }
        public string password { get; set; }    
        public string password_encrypted { get; set; }
        public string token { get; set; }
        public int count { get; set; }

    }
}
