﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Additive
    {
        public int additives_id { get; set; }
        public string ins_id { get; set; }
        public string additives_description { get; set; }

    }
}
