﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class Functional
    {
        public int functional_id { get; set; }
        public string functional_description { get; set; }

    }
}
