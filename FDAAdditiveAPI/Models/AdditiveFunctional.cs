﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FDAAdditiveAPI.Models
{
    public class AdditiveFunctional
    {
        public int additives_id { get; set; }
        public string ins_id { get; set; }
        public string additives_description { get; set; }
        public string functional_description { get; set; }
        public int functional_id { get; set; }


    }
}
