import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-additive-data',
  templateUrl: './additive-data.component.html',
  styleUrls: ['./additive-data.component.css']
})
export class AdditiveDataComponent implements OnInit {
  public additives: Additives[];

  dtOptions: DataTables.Settings = {};
  showModal: boolean;
  additives_id: number;
  ins_id: string;
  additives_description: string;

  onClick(event) {
    this.showModal = true; // Show-Hide Modal Check
    this.additives_id = event.target.id;
    this.ins_id = document.getElementById("ins_id"+this.additives_id).innerHTML;
    this.additives_description = document.getElementById("additives_description"+this.additives_id).innerHTML;
  }

  hide() {
    this.showModal = false;
  }


  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Additives[]>('http://localhost:5000/api/v1/additives').subscribe(result => {
      this.additives = result;
    }, error => console.error(error));
  }


  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
}

interface Additives {
  additives_id: number;
  ins_id: string;
  additives_description: string;
}
