export class AdditiveFunctional {
  additives_id: number;
  ins_id: string;
  additives_description: string;
  functional_description: string;
}
