export class Group {
  addictives_id: number;
  ins_id: string;
  addictives_description: string;
  functional_description: string;
  groups_id: number;
}
