export class User {
  userID: number;
  fullname: string;
  username: string;
  password: string;
  password_encrypted: string;
  token: string;
  count: number;
}
