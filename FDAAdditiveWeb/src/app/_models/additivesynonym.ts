export class AdditiveSynonym {
  additives_id: number;
  ins_id: string;
  additives_description: string;
  additives_synonyms: string;
  groups_description: string;
}
