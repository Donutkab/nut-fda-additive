export class AdditiveFood {
  additives_id: number;
  foods_id: number;
  ins_id: string;
  additives_description: string;
  foods_code: string;
  foods_name: string;
  foods_description_th: string;
  foods_description_en: string;
  max_level: string;
  years: string;
  addictives_description: string;
}
