import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdditiveSynonymService } from '../_services/additivesynonym.service';
import { AdditivegroupService } from '../_services/additivegroup.service';
import { AdditiveSynonym } from '../_models/additivesynonym';
import { Additivegroup } from '../_models/additivegroup';
import { AdditiveService } from '../_services/additive.service';
import { Additive } from '../_models/additive';

@Component({
  selector: 'app-additive-groupname-synonym',
  templateUrl: './additive-groupname-synonym.component.html',
  styleUrls: ['./additive-groupname-synonym.component.css']
})
export class AdditiveGroupnameSynonymComponent implements OnInit {

  additiveinfo: Additive[];
  synonymssearchs: AdditiveSynonym[];
  additivegroups: Additivegroup[];
  additivesynonyms: AdditiveSynonym[];
    id: number;

  constructor(private route: ActivatedRoute, private additiveSynonymService: AdditiveSynonymService, private additivegroupService: AdditivegroupService, private additiveService: AdditiveService) { }

  dtOptions: DataTables.Settings = {};

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };

    //this.id = this.route.snapshot.params['id'];
    //alert(this.id);


    let value = this.route.snapshot.queryParams['textSearch']
    this.additiveService.GetAdditiveDescriptionDetailsData(value)
      .subscribe(data => {
        this.additiveinfo = data;
      });
    let value_group = this.route.snapshot.queryParams['textSearch']
    this.additivegroupService.GetDistinctAdditiveGroupByGroupDescription(value_group)
      .subscribe(data => {
        this.additivegroups = data;
      });
    let value_synonym = this.route.snapshot.queryParams['textSearch']
    this.additiveSynonymService.GetAdditiveSynonymByAdditiveSynonyms(value_synonym)
      .subscribe(data => {
        this.additivesynonyms = data;
      });


 
  }
}
