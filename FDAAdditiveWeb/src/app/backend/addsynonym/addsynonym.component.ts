import { Component, OnInit } from '@angular/core';
import { AdditiveSynonymService } from '../../_services/additivesynonym.service';
import { ActivatedRoute } from '@angular/router';
import { AdditiveSynonym } from '../../_models/additivesynonym';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-addsynonym',
  templateUrl: './addsynonym.component.html',
  styleUrls: ['./addsynonym.component.css']
})
export class AddsynonymComponent implements OnInit {

  id: number;
  SynonymList: AdditiveSynonym;
  dropdown1: any;
  sub: any;
  addNoteData: AdditiveSynonyms;


  constructor(private additiveSynonymService: AdditiveSynonymService, private route: ActivatedRoute) { }

  ngOnInit() {


    this.sub = this.route.params.subscribe(params => {
      this.id = parseInt(params['id']);
 
    })


    this.additiveSynonymService.GetAllSynonym().subscribe(
      data => {
        this.SynonymList = data;
        let arrtest;
        let arr = [];
        Object.keys(data).map(function (key) {
          arr.push({ [key]: data[key] })
          return arr;
        }
        );

        //console.log('Object=',data);
        //console.log('Array=', arr)

        let size: number;

        size = arr.length;
        //alert(size);

        arrtest = JSON.stringify(data);
        //console.log(arrtest);
        //alert(arrtest);
      }
    )
  }




  AddSynonymListData(f: NgForm) {
    //alert("OK");
    this.addNoteData = f.value
    //console.log(f.value);
    //alert(JSON.stringify(f.value.notesdescription));

    let doc = {
      additives_id: this.id,
      additives_synonyms: f.value.synonymdata
    }
     //alert(JSON.stringify(doc));
    console.log(doc);
    //alert(JSON.stringify(doc));
    this.additiveSynonymService.AddSynonymData(doc).subscribe(
      data => {
        //alert(JSON.stringify(data));
        window.location.reload();
      }
    )
    //alert(JSON.stringify(this.addFunctionalData));
    //this.functionalService.AddDescriptionFunctional(this.addFunctionalData).subscribe(data => {
    //  window.location.reload();
    //  this.functional_id = data.functional_id;
    //  this.functional_description = data.functional_description;
    //});
  }




  //powers = ['Really Smart', 'Super Flexible', 'Weather Changer'];
  //hero: string

  //ClickAddDes() {
  //  alert('ok');
  //}



  form = new FormGroup({
    website: new FormControl('', Validators.required)
  });

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
  }
  changeWebsite(e) {
    console.log(e.target.value);
  }

}


interface AdditiveSynonyms {
  additives_id: number;
  //ins_id: string;
  //additives_description: string;
  additives_synonyms: string;
  //groups_description: string;
}
