import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsynonymComponent } from './addsynonym.component';

describe('AddsynonymComponent', () => {
  let component: AddsynonymComponent;
  let fixture: ComponentFixture<AddsynonymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsynonymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsynonymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
