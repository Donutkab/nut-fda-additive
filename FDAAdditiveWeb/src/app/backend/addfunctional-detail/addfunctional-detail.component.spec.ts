import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddfunctionalDetailComponent } from './addfunctional-detail.component';

describe('AddfunctionalDetailComponent', () => {
  let component: AddfunctionalDetailComponent;
  let fixture: ComponentFixture<AddfunctionalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddfunctionalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddfunctionalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
