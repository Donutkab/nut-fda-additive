import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { FunctionalService } from '../../_services/functional.service';
import { Additive } from '../../_models/additive';
import { AdditiveFunctionalService } from '../../_services/additivefunctional.service';
import { AdditiveFunctional } from '../../_models/additivefunctional';
import { Functional } from '../../_models/functional';

@Component({
  selector: 'app-addfunctional-detail',
  templateUrl: './addfunctional-detail.component.html',
  styleUrls: ['./addfunctional-detail.component.css']
})
export class AddfunctionalDetailComponent implements OnInit {
  id: number;
  additives: Additive;
    sub: any;
  dropdown1: any;
  functional_id: number;
    peopleForm: any;
  groups: any;
  websiteList: any = ['ItSolutionStuff.com', 'HDTuto.com', 'Nicesnippets.com']

  constructor(private route: ActivatedRoute, private router: Router, private functionalService: FunctionalService, private additiveFunctionalService: AdditiveFunctionalService) { }

  addFunctionalData: AdditiveFunctional;
  FunctionalDropdownData : Functional;

    ngOnInit() {

      this.sub = this.route.params.subscribe(params => {
        this.id = parseInt(params['id']);
    });


      this.functionalService.GetAllFunctional().subscribe(
        data => this.FunctionalDropdownData = data
      )
    //console.log(this.id);
    //alert(this.id);

    //  this.additiveFunctionalService.GetAdditiveFunctionalsByAdditiveID(this.id).subscribe(
    //  data => {
    //    //console.log(data.functional_description);
    //    //alert(JSON.stringify(data));
    //    this.addFunctionalData = data;
    //    console.log(this.addFunctionalData);
    //  }
    //)

 
  }


  powers = ['Really Smart', 'Super Flexible', 'Weather Changer'];

  hero = { name: 'Dr.', alterEgo: 'Dr. What', power: this.powers[0] };


  form = new FormGroup({
    website: new FormControl('', Validators.required)
  });

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
  }
  changeWebsite(e) {
    console.log(e.target.value);
  }


   parent_id: number;

  onOptionsSelected(event) {
    const value = event.target.value;
    this.dropdown1 = parseInt(value);
    //alert(this.dropdown1);
    //console.log(this.dropdown1);

    if (this.dropdown1 == undefined) {

      //this.dropdown1 = null;
        
      alert(this.dropdown1);
      console.log(this.dropdown1);

    }
    else
      this.dropdown1 = this.functional_id;
    console.log(this.dropdown1);
  }


  ClickAddDes(f: NgForm) {
    //alert("OK");
    this.addFunctionalData = f.value
    //console.log(this.id);
    //alert(JSON.stringify(this.addFunctionalData));
 
    let doc = {
      additives_id: this.id,
      functional_description: f.value.functionaldescription
      
    }
    alert(doc);
    console.log(doc);
    //alert(JSON.stringify(doc));
    this.additiveFunctionalService.AddAdditiveFunctionalsByAdditiveID(doc).subscribe(
      data => {
        alert(JSON.stringify('ok'));
      }
    )
    //alert(JSON.stringify(this.addFunctionalData));
    //this.functionalService.AddDescriptionFunctional(this.addFunctionalData).subscribe(data => {
    //  window.location.reload();
    //  this.functional_id = data.functional_id;
    //  this.functional_description = data.functional_description;
    //});
  }

 
 


  //gotoNote() {
  //  //alert('ok');
  //  this.router.navigate(['/backend/addnotes'])

  //}

}
interface Functionalsdata {
  addictives_id: number;
  ins_id: string;
  addictives_description: string;
  //functional_id: number;
  functional_description: string;
}
