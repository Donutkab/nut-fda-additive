import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 import { AdditiveFunctional } from '../../_models/additivefunctional';
 import { Router, ActivatedRoute } from '@angular/router';
import { AdditiveService } from '../../_services/additive.service';
import { AdditiveFunctionalService } from '../../_services/additivefunctional.service';
import { AdditiveFoodService } from '../../_services/additivefood.service';
import { AdditiveSynonym } from '../../_models/additivesynonym';
import { AdditiveSynonymService } from '../../_services/additivesynonym.service';
import { Additive } from '../../_models/additive';
import { AdditiveFood } from '../../_models/additivefood';

@Component({
  selector: 'app-backend-additive-detail',
  templateUrl: './additive-detail.component.html',
  styleUrls: ['./additive-detail.component.css']
})
export class BackendAdditiveDetailComponent implements OnInit{

  id: number;
  additives: Additive;
  additivefunctionals: AdditiveFunctional;
  additivefoods: AdditiveFood;
  additivesynonyms: AdditiveSynonym;
  insid: string;
  additivesdata: any = [];
  constructor(private route: ActivatedRoute, private router: Router, private additiveService: AdditiveService, private additivefunctionalService: AdditiveFunctionalService, private additivefoodService: AdditiveFoodService, private additivesynonymService: AdditiveSynonymService) { }

  async ngOnInit() {


    let value = this.route.snapshot.queryParams['id']
   

    if (value != null) {
      await this.additiveService.GetAdditiveInsDetailsData(value)
        .subscribe(async call1Response => {
          
        this.insSearch(call1Response.additives_id);
          
        });
    } else {
      this.insSearch( this.route.snapshot.params['id']);
    }

  }


  insSearch(valueIns) {
    this.id = valueIns;
    //alert(this.id);
    //console.log(this.id);
    this.additiveService.GetAdditiveDataById(this.id)
      .subscribe(data => {
        //console.log(data)
        this.additives = data;
        //this.router.navigate(['/backend/addfunctional-detail'], { queryParams: { token: this.additives.additives_id } })

      }, error => console.log(error));

    this.additivefunctionalService.GetAddictiveFunctionalData(this.id)
      .subscribe(data => {
        //console.log(data)
        this.additivefunctionals = data;
        //alert(JSON.stringify(this.additivefunctionals));
      }, error => console.log(error));

    this.additivefoodService.GetAdditiveFood(this.id)
      .subscribe(data => {
        //console.log(data)
        this.additivefoods = data;
      }, error => console.log(error));

    this.additivesynonymService.GetAdditiveSynonymData(this.id)
      .subscribe(data => {
        //console.log(data)
        this.additivesynonyms = data;
      }, error => console.log(error));
  }

}

