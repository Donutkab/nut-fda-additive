import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FunctionalService } from '../../_services/functional.service';
import { Functional2 } from '../../_models/functional2';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-backend-functionals',
  templateUrl: './functionals.component.html',
  styleUrls: ['./functionals.component.css']
})
export class BackendFunctionalsComponent implements OnInit {
  public functionals: Functionals[];
  functional_id: number;
  functional_description: string;
  save: Functional2;
  add: Functional2;
  funitional2: Functional2;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private functionalService: FunctionalService) {
    http.get<Functionals[]>('http://localhost:5000/api/v1/Functional').subscribe(result => {
      this.functionals = result;
    }, error => console.error(error));
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
  ClickAddDes(f: NgForm) {
    alert("OK");
    this.add = f.value
    this.functionalService.AddDescriptionFunctional(this.add).subscribe(data => {
      window.location.reload();
      this.functional_id = data.functional_id;
      this.functional_description = data.functional_description;
    });
  }
  ClickEdit(functional_id: number) {
      this.functionalService.GetFunctionalDataByID(functional_id).subscribe(data => {
      this.functional_id = data.functional_id;
      this.functional_description = data.functional_description;
    });
  }

  ClickEditSave() {
    let doc = {
      functional_id: this.functional_id,
      functional_description: this.functional_description
    }
    this.save = doc;
    this.functionalService.EditDescriptionFunctional(doc.functional_id,this.save).subscribe(data => {
      window.location.reload();
    },
      error => {
        console.error("Error");
      }
    )
  }

  //test() {
  //  alert('kk');
  //}
}

interface Functionals {
  addictives_id: number;
  ins_id: string;
  addictives_description: string;
  functional_description: string;
}

