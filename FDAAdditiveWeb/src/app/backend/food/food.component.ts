import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FoodService } from '../../_services/food.service';
import { Food } from '../../_models/food';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-backend-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class BackendFoodComponent implements OnInit {
  public foods: Food[];
  foods_id: number;
  selected: any;
  selected2: any;
  selected3: any;


  model = new Food();



  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private foodService: FoodService) {
    http.get<Foods[]>('http://localhost:5000/api/v1/Food').subscribe(result => {
      this.foods = result;
    }, error => console.error(error));
  }

  orders: Foods;
  orders2: Foods;
  orders3: Foods;
  orders4: Foods;
  orders5: Foods;

  additives: FoodDropdown[];

  Idfood: number;
  foodcode: string;
  foodname: string;

  ordersdata: Foods;
  dtOptions: DataTables.Settings = {};

   ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };

     

    //this.foodService.GetFoodParentIdList().subscribe(
    //  data => {
    //    this.orders = data;
    //    //alert(JSON.stringify(this.orders));
    //  }

     //)
     this.foods_id = 0;
     this.foodService.GetAllFoodByParentIdForDropdown(this.foods_id).subscribe(
       data => {
         this.orders = data;
          //alert(JSON.stringify(this.orders.foods_id));



       }


     )


     //this.foodService.GetAllFoodByParentIdForDropdown(this.foods_id).subscribe(
     //  data => {
     //    this.ordersdata = data;
     //    alert(JSON.stringify(this.ordersdata));
     //  }
     //  )
  }



  //onSubmitbth(form) {
  //  console.log(form.value)


  //}

 
//  if(dropdown1 == '0') {
//  parent_id = '0'
//} elseif(dropdown1 != '') {
//  parent_id = dropdown1.foodsid
//} elseif(dropdown1 != '') and(dropdown2 != '') {
//  parent_id = dropdown2.foodsid
//} elseif(dropdown1 != '') and(dropdown2 == '0') {
//  parent_id = dropdown1.foodsid
//} elseif(dropdown1 != '') and(dropdown2 != '') and(dropdown3 != '') {
//  parent_id = dropdown3.foodsid
//} elseif(dropdown1 != '') and(dropdown2 != '') and(dropdown3 == '0') {
//  parent_id = dropdown2.foodsid
//}

  parent_id: number;

  //async onOptionsSelectedtest(event) {
  //  //alert(event.target.value);
  //  const value = event.target.value;
  //  this.dropdown1 = value;

  //  if (this.dropdown1 == 0) {
  //    this.parent_id = 0;
  //    alert(this.parent_id);
  //  }

  //  await this.foodService.GetAllFoodByParentIdForDropdown(this.selected).subscribe(
  //    data => {
  //      //this.orders = data;
  //      //alert(JSON.stringify(data));

  //      this.orders2 = data;
  //      //alert(JSON.stringify(this.orders2));

  //    }
  //  );
  //}

  dropdown1: any;
  dropdown2: any;
  dropdown3: any;
  dropdown4: any;

  //async onOptionsSelected(event) {
  //  //alert(event.target.value);
  //  const value = event.target.value;
  //  this.dropdown1 = value;
  //  //alert(this.selected);

  //    if (this.dropdown1 == 0) {
  //    this.parent_id = 0;
  //      //alert(this.parent_id);
  //      console.log(this.parent_id);

  //    }
  //      else if(this.dropdown1 != '') {
  //      this.parent_id = this.dropdown1;
  //      //alert(this.parent_id);
  //      console.log(this.parent_id);

  //      }
   

  
  //  await this.foodService.GetAllFoodByParentIdForDropdown(this.parent_id).subscribe(
  //    data => {
  //      //this.orders = data;
  //      //alert(JSON.stringify(data));

  //      this.orders2 = data;
  //      //alert(JSON.stringify(this.orders2));
  //      console.log(this.orders2);

  //    }
  //  );

 

  //}


  async onOptionsSelected(event) {
    //alert(event.target.value);
    const value = event.target.value;
    this.dropdown1 = value;
    //alert(this.dropdown1);

    if (event == 'undefined') {
      alert('ok');
      this.parent_id = 0;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }

    else if (this.dropdown1 == 'undefined') {
      alert('ok');
      this.parent_id = 0;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }
    else {
      this.parent_id = this.dropdown1;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }



    await this.foodService.GetAllFoodByParentIdForDropdown(this.parent_id).subscribe(
      data => {
        //this.orders = data;
        //alert(JSON.stringify(data));

        this.orders2 = data;
        //alert(JSON.stringify(this.orders2.foods_id));
        //console.log(this.orders2);

      }
    );



  }
  async onOptionsSelected1(event) {
    //alert(event.target.value);
    const value = event.target.value;
    this.dropdown2 = value;
    //alert(this.selected);


    if (this.dropdown2 == undefined) {
      this.parent_id = 0;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }
    else {
      this.parent_id = this.dropdown2;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }
    await this.foodService.GetAllFoodByParentIdForDropdown(this.parent_id).subscribe(
      data => {
        //this.orders = data;
        //alert(JSON.stringify(data));

        this.orders3 = data;
        //console.log(this.orders3);

        //alert(JSON.stringify(this.orders3));

      }
    );

  }


  async onOptionsSelected3(event) {
    //alert(event.target.value);
    const value = event.target.value;
    this.dropdown3 = value;
    //alert(this.selected);


    if (this.dropdown3 == undefined) {
      this.parent_id = 0;
      //alert(this.parent_id);
    }

    else   {
      this.parent_id = this.dropdown3;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }


    await this.foodService.GetAllFoodByParentIdForDropdown(this.parent_id).subscribe(
      data => {
        //this.orders = data;
        //alert(JSON.stringify(data));

        this.orders4 = data;
        //console.log(this.orders4);

        //alert(JSON.stringify(this.orders3));

      }
    );

  }
 

  async onOptionsSelected4(event) {
    //alert(event.target.value);
    const value = event.target.value;
    this.dropdown4 = value;
    //alert(this.selected);


    if (this.dropdown4 == undefined) {
      this.parent_id = 0;
      //alert(this.parent_id);
    }

    else   {
      this.parent_id = this.dropdown4;
      //alert(this.parent_id);
      //console.log(this.parent_id);

    }

    await this.foodService.GetAllFoodByParentIdForDropdown(this.parent_id).subscribe(
      data => {
        //this.orders = data;
        //alert(JSON.stringify(data));

        this.orders5 = data;
        //alert(JSON.stringify(this.orders3));

      }
    );

  }

  doc: FoodDropdown;
  onSubmit(f: NgForm) {
    //console.log(f.value);  // { first: '', last: '' }
    //console.log(f.valid);  // false
    alert('ok');
    this.additives = f.value;
    console.log(JSON.stringify(this.additives)); 
    alert(JSON.stringify(this.additives));


    let doc = {
      //additives_id: this.test,
      //additives_description: this.test1,
      //ins_id: this.test2,

      foods_code: f.value.foods_code,
      foods_name: f.value.foods_name,
      foods_description_th: f.value.foods_description_th,
      foods_description_en: f.value.foods_description_en,
      parent_id: parseInt(f.value.first),
      parent_id_1: parseInt(f.value.first2),
      parent_id_2: parseInt(f.value.first3),
      parent_id_3: parseInt(f.value.first4)

    }
    if (f.value.first == null) {

      doc.parent_id = 0; 
    }

    if (f.value.first2 == null) {

      doc.parent_id_1 = 0;
    }

    if (f.value.first3 == null) {

      doc.parent_id_2 = 0;
    }

    if (f.value.first4 == null) {

      doc.parent_id_3 = 0;
    }
    //this.functionEdit = doc;
    console.log(doc);
    //alert(JSON.stringify(doc));

    this.foodService.AddFood(doc).subscribe(
      data => {
        alert('ok');

        //alert(JSON.stringify(data));
        //f.reset();
        window.location.reload();
      }
    )


    //alert('ok');
    //alert(JSON.stringify(this.additives));
    //console.log(this.functional);
    //window.location.reload();


    //this.additiveService.postFunctional(this.additives).subscribe(
    //  data => {
    //    alert(JSON.stringify(data));

    //    f.reset();
    //    window.location.reload();
    //  }
    //)



  }


  onEditButton(id: number) {
    //alert('ok');
    this.foodService.GetFoodById(id).subscribe(
      data=>{
        this.Idfood = data.foods_id;
        this.foodcode= data.foods_code;
        this.foodname = data.foods_name;
        
      })
  }
   UpdateData: Foods;
  UpdateButton() {
    let doc = {
      foods_id: this.Idfood,
      foods_code: this.foodcode,
      foods_name: this.foodname,
      foods_description_th: undefined,
      foods_description_en: undefined,
      parent_id: undefined
    }
    this.UpdateData = doc;
    //this.foodService.

  }


}

interface Foods {
  foods_id: number;
  foods_code: string;
  foods_name: string;
  foods_description_th: string;
  foods_description_en: string;
  parent_id: number;
}


interface FoodDropdown {

  foods_code: string;
  foods_name: string;
  foods_description_th: string;
  foods_description_en: string;
  parent_id: number;
  parent_id_1: number;
  parent_id_2: number;
  parent_id_3: number;
}
