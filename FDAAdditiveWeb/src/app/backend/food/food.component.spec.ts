import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackendAdditiveComponent } from './food.component';

describe('BackendIndexComponent', () => {
  let component: BackendAdditiveComponent;
  let fixture: ComponentFixture<BackendAdditiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BackendAdditiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackendAdditiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
