import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { AdditiveService } from '../../_services/additive.service';

@Component({
  selector: 'app-backend-additive',
  templateUrl: './additive.component.html',
  styleUrls: ['./additive.component.css']
})
export class BackendAdditiveComponent implements OnInit {
  public additives: Additives[];
  public additivesdata: Additives;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private additiveService: AdditiveService) {
    http.get<Additives[]>('http://localhost:5000/api/v1/additives').subscribe(result => {
      this.additives = result;
    }, error => console.error(error));
  }
  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }

  onSubmit(f: NgForm) {
    this.additivesdata = f.value;
    //alert(JSON.stringify(this.additives));
    //alert('ok');
    console.log(this.additivesdata);
    this.additiveService.AddAdditive(this.additivesdata).subscribe(
      data => {
        alert(JSON.stringify(data));

        f.reset();
        window.location.reload();
      }
    )

  }


  test: number = null;
  test1: string = 'oktest';
  test2: string = null;

  onEditButton(id: number) {
    this.additiveService.GetAdditiveDataById(id).subscribe(
      data => {
        this.test = data.additives_id;
        this.test2 = data.ins_id;
        this.test1 = data.additives_description;
        //alert(JSON.stringify(data));
      }
    )
  }


  functionEdit: Additives;


  UpdateButton() {

    let doc = {
      additives_id: this.test,
      additives_description: this.test1,
      ins_id: this.test2
    }
    this.functionEdit = doc;

    this.additiveService.UpdateAdditive(doc.additives_id, this.functionEdit).subscribe(
      data => {
        alert('Do you sure to update data');
        window.location.reload();
      },
      error => {
        console.error("Error update data!");

      }

    )


    //this.functionEdit.functionalId = this.test;
    //this.functionEdit.functionalDescription = this.test1;
    //let id = this.test;
    console.log(this.functionEdit);
    //alert(doc);

    //this.functionalService.putFunctional(doc.functionalId, this.functionEdit).subscribe(
    //  data => {
    //    // refresh the list
    //    alert('Do you sure to update data');
    //    window.location.reload();
    //  },
    //  error => {
    //    console.error("Error saving food!");

    //  }
    //);
  }


}

interface Additives {
  additives_id: number;
  ins_id: string;
  additives_description: string;
}
