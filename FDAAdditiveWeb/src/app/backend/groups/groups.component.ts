import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GroupService } from '../../_services/group.service';
import { Group } from '../../_models/Group';
import { NgForm } from '@angular/forms';
import { Group2 } from '../../_models/group2';

@Component({
  selector: 'app-backend-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class BackendGroupsComponent implements OnInit {
  public groups: Groups[];
  groups_id: number;
  groups_description: string;
  save: Group2;
  add: Group2;
  funitional2: Group2;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string,private groupService : GroupService) {
    http.get<Groups[]>('http://localhost:5000/api/v1/Groups').subscribe(result => {
      this.groups = result;
    }, error => console.error(error));
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
  ClickAddDes(f: NgForm) {
    this.add = f.value
    alert(JSON.stringify(this.add))

    this.groupService.AddDescriptionFunctional(this.add).subscribe(data => {
      window.location.reload();
      this.groups_id = data.groups_id;
      this.groups_description = data.groups_description;
    });
  }
  ClickEdit(groups_id: number) {
    this.groupService.GetGroupDataByID(groups_id).subscribe(data => {
      this.groups_id = data.groups_id;
      this.groups_description = data.groups_description;
    });
  }

  ClickEditSave() {
    alert('k')
    let doc = {
      groups_id: this.groups_id,
      groups_description: this.groups_description
    }
    this.save = doc;
    this.groupService.EditDescriptionFunctional(doc.groups_id, this.save).subscribe(data => {
      window.location.reload();
    },
      error => {
        console.error("Error");
      }
    )
  }

}

interface Groups {
  addictives_id: number;
  ins_id: string;
  addictives_description: string;
  functional_description: string;
  groups_id: number;
}
