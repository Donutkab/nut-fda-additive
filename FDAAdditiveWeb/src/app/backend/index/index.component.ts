import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../_models';

@Component({
  selector: 'app-backend-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  fullname: string;
  userdata: User;
  text: 'ok';
  constructor(private route: ActivatedRoute) { }
 
  ngOnInit() {
    let value = this.route.snapshot.queryParams['textSearch']
    this.userdata = value;
    //alert(value);
    this.fullname = this.userdata.fullname;
    console.log(this.fullname);
  }

}
