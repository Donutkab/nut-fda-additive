import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NoteService } from '../../_services/note.service';
import { NgForm } from '@angular/forms';
import { Note2 } from '../../_models/note2';
import { Note } from '../../_models/note';

@Component({
  selector: 'app-backend-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class BackendNotesComponent implements OnInit {
  public notes: NoteIn[];
  add: Note;
  save: Note;
  notes_id: number;
  notes_code: string;
  notes_description: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private noteservice: NoteService) {
    http.get<NoteIn[]>('http://localhost:5000/api/v1/Notes').subscribe(result => {
      this.notes = result;
    }, error => console.error(error));
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
  ClickAddDes(f: NgForm) {
    alert("eoeoeoeo")
    this.add = f.value
    alert(JSON.stringify(this.add))

    this.noteservice.AddDescription_Code_Note(this.add).subscribe(data => {
      window.location.reload();
      this.notes_id = data.notes_id;
      this.notes_code = data.notes_code;
      this.notes_description = data.notes_description;
    });
  }
  ClickEdit(notes_id: number) {
    this.noteservice.GetNoteDataByID(notes_id).subscribe(data => {
      this.notes_id = data.notes_id;
      this.notes_code = data.notes_code;
      this.notes_description = data.notes_description;
    });
  }
  ClickEditSave() {
    alert("ASdasdasd")
    let doc = {
      notes_id: this.notes_id,
      notes_code: this.notes_code,
      notes_description: this.notes_description
    }
    this.save = doc;
    this.noteservice.EditDescription_Code_Note(doc.notes_id,this.save).subscribe(data => {
      window.location.reload();
    },
      error => {
        console.error("Error");
      }
    )
  }
}

interface NoteIn {
  notes_id: number;
  notes_code: string;
  notes_description: string;
}
