import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { NoteService } from '../../_services/note.service';
import { Note } from '../../_models/note';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addnote',
  templateUrl: './addnote.component.html',
  styleUrls: ['./addnote.component.css']
})
export class AddnoteComponent implements OnInit {
  id: number;
  NoteList: Note;
  dropdown1: any;
  sub: any;
  addNoteData: Note;


  constructor(private noteService: NoteService, private route: ActivatedRoute) { }

  ngOnInit() {


    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    })
  

    this.noteService.GetAllNote().subscribe(
      data => {
        this.NoteList = data;
        let arrtest  ;
        let arr = [];
        Object.keys(data).map(function (key) {
          arr.push({ [key]: data[key] })
          return arr;
        }
        );

        //console.log('Object=',data);
        //console.log('Array=', arr)

        let size: number;

        size = arr.length;
        //alert(size);

        arrtest = JSON.stringify(data);
        console.log(arrtest);
        alert(arrtest);
      }
    )
  }


 

  AddNoteData(f: NgForm) {
    //alert("OK");
    this.addNoteData = f.value
    //console.log(f.value);
    //alert(JSON.stringify(f.value.notesdescription));

    let doc = {
      notes_id: undefined,
      notes_code: 'kk',
      notes_description: f.value.notesdescription
    }
   // alert(JSON.stringify(doc));
    //console.log(doc);
    //alert(JSON.stringify(doc));
    this.noteService.AddDescription_Code_Note(doc).subscribe(
      data => {
        alert(JSON.stringify('ok'));
      }
    )
    //alert(JSON.stringify(this.addFunctionalData));
    //this.functionalService.AddDescriptionFunctional(this.addFunctionalData).subscribe(data => {
    //  window.location.reload();
    //  this.functional_id = data.functional_id;
    //  this.functional_description = data.functional_description;
    //});
  }




  //powers = ['Really Smart', 'Super Flexible', 'Weather Changer'];
  //hero: string

  //ClickAddDes() {
  //  alert('ok');
  //}



  form = new FormGroup({
    website: new FormControl('', Validators.required)
  });

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
  }
  changeWebsite(e) {
    console.log(e.target.value);
  }

}
