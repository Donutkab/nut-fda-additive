import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Additive } from '../_models/additive';

@Injectable({ providedIn: 'root' })

export class AdditiveService {

  private baseUrl = 'http://localhost:5000/api/v1/additives';
  private baseUrlInsDetails = 'http://localhost:5000/api/v1/additives/ins';
  private baseUrlDescriptionDetails = 'http://localhost:5000/api/v1/additives/description';
  private baseUrlAddAdditive = 'http://localhost:5000/api/v1/additives/api/v1/additive/create';
  private baseUrlUpdateAdditive = ' http://localhost:5000/api/v1/additives/api/v1/additive/update?id=';

 

 
  //private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  //headers = new HttpHeaders({ "content-type": "application/json", "Accept": "application/json" });



  constructor(private http: HttpClient) { }

  GetAdditiveDataById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  GetAdditiveInsDetailsData(ins: string): Observable<any> {
    return this.http.get(`${this.baseUrlInsDetails}/${ins}`);
  }

  GetAdditiveDescriptionDetailsData(additives_description: string): Observable<any> {
    return this.http.get(`${this.baseUrlDescriptionDetails}/${additives_description}`);
  }

  //AddAdditivesData(additives_description: string): Observable<any> {
  //  return this.http.get(`${this.baseUrlAddAdditive}/${additives_description}`);
  //}

  AddAdditive(additive: Additive) {
    return this.http.post<Additive[]>(this.baseUrlAddAdditive, additive);
  }

  UpdateAdditive(id: number,additive: Additive) {
    return this.http.put<Additive>(this.baseUrlUpdateAdditive+id, additive);
  }
  

 
  }


