import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../_models/Group';
import { Group2 } from '../_models/group2';

@Injectable({providedIn: 'root'})
export class GroupService {
  private baseUrl = 'http://localhost:5000/api/v1/groups/groupcode';
  private baseUrlAll = 'http://localhost:5000/api/v1/groups';
  private baseUrlGroup = 'http://localhost:5000/api/v1/groups/details';
  private AddDescription = 'http://localhost:5000/api/v1/groups/api/v1/Group/create';
  private EditDescription = 'http://localhost:5000/api/v1/groups/api/v1/Group/update?group_id=';
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  headers = new HttpHeaders({ "content-type": "application/json", "Accept": "application/json" });
  constructor(private http: HttpClient) { }

  GetAdditiveGroupByGroupDescription(groups_description: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${groups_description}`);
  }
  GetGroupDataByID(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlAll}/${id}`);
  }



  AddDescriptionFunctional(AddDes: Group2) {
    return this.http.post<Group2>(this.AddDescription, AddDes)
  }
  EditDescriptionFunctional(groups_id: number, EditDes: Group2) {
    return this.http.put<Group2>(this.EditDescription + groups_id, EditDes)
  }

}
