import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
  private baseUrlUserLogin = 'http://localhost:5000/api/User/api/v1/Users/login';


  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  headers = new HttpHeaders({ "content-type": "application/json", "Accept": "application/json" });
  constructor(private http: HttpClient) { }




  UserLogin(user: User) {
    return this.http.post<User>(this.baseUrlUserLogin, user);
  }

  getAll() {
    return this.http.get<User[]>(`/users`);
  }
}
