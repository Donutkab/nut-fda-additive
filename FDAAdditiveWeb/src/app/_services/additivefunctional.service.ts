import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdditiveFunctional } from '../_models/additivefunctional';

@Injectable({ providedIn: 'root' })

export class AdditiveFunctionalService {

  private baseUrl = 'http://localhost:5000/api/v1/additivefunctionals';
  private baseUrlFunctional = 'http://localhost:5000/api/v1/additivefunctionals';
  private baseUrlSynonym  = 'http://localhost:5000/api/v1/additivesynonym';
  private baseUrlGetAdditiveFunctionalsByAdditiveID = 'http://localhost:5000/api/v1/additivefunctionals';
  private baseUrlAddAdditiveFunctionalsByAdditiveID = 'http://localhost:5000/api/v1/additivefunctionals/api/v1/additivefunctional/create';
  
  


  constructor(private http: HttpClient) { }

  GetAddictiveFunctionalData(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  GetAdditiveFunctionalsByFunctionalID(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlFunctional}/${id}`);
  }


  GetAdditiveFunctionalsByAdditiveID(foods_id: number) {
    return this.http.get<AdditiveFunctional>(this.baseUrlGetAdditiveFunctionalsByAdditiveID + "/" + foods_id);
  }

  AddAdditiveFunctionalsByAdditiveID(additiveFunctional: Functionalsdata) {
    return this.http.post<Functionalsdata[]>(this.baseUrlAddAdditiveFunctionalsByAdditiveID, additiveFunctional);
  }
}

interface Functionalsdata {
  additives_id: number;
  functional_description: string;
}
