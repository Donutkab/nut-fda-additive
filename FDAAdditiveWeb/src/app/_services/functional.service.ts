import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Functional } from '../_models/functional';
import { Functional2 } from '../_models/functional2';

@Injectable({ providedIn: 'root' })

export class FunctionalService {

  private baseUrl = 'http://localhost:5000/api/v1/AddictiveFunctional/DetailsByFunctionalID';
  private baseUrlAll = 'http://localhost:5000/api/v1/Functional';
  private baseUrlFunctional = 'http://localhost:5000/api/v1/additivefunctionals/details';
  private AddDescription = 'http://localhost:5000/api/v1/Functional/api/v1/Functional/create';
  private EditDescription = 'http://localhost:5000/api/v1/Functional/api/v1/Functional/update?functional_id=';
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  headers = new HttpHeaders({ "content-type": "application/json", "Accept": "application/json" });
  constructor(private http: HttpClient) { }

  GetFunctionalData(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  GetFunctionalDataByID(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlAll}/${id}`);
  }

  GetAllFunctional(): Observable<any> {
    return this.http.get(`${this.baseUrlAll}/`);
  }
  GetAdditiveFunctionalsByFunctionalID(id:number): Observable<any> {
    return this.http.get(`${this.baseUrlFunctional}/${id}`);
  }
  AddDescriptionFunctional(AddDes: Functional2) {
    return this.http.post<Functional2>(this.AddDescription, AddDes)
  }
  EditDescriptionFunctional(functional_id: number, EditDes: Functional2) {
    return this.http.put<Functional2>(this.EditDescription + functional_id, EditDes)
  }
}

