import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdditivegroupService {

  private baseUrl = 'http://localhost:5000/api/v1/additivegroups/groupdetail';

  constructor(private http: HttpClient) { }

  GetDistinctAdditiveGroupByGroupDescription(groups_description: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${groups_description}`);
  }
}
