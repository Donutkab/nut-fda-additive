import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Note } from '../_models/note';
import { Note2 } from '../_models/note2';

@Injectable({  providedIn: 'root'})
export class NoteService {
  private baseUrl = 'http://localhost:5000/api/v1/notes/notecode';
  private baseUrlAll = 'http://localhost:5000/api/v1/notes';
  private baseUrlFunctional = 'http://localhost:5000/api/v1/groups/details';
  private AddDescription = 'http://localhost:5000/api/v1/notes/api/v1/Note/create';
  private EditDescription = 'http://localhost:5000/api/v1/notes/api/v1/Note/update?note_id=';
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  headers = new HttpHeaders({ "content-type": "application/json", "Accept": "application/json" });
  constructor(private http: HttpClient) { }


  GetAllNote() {
    return this.http.get<Note>(this.baseUrlAll);
  }

  GetAdditiveGroupByGroupDescription(groups_description: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${groups_description}`);
  }
  GetNoteDataByID(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlAll}/${id}`);
  }

  AddDescription_Code_Note(AddDes: Note) {
    return this.http.post<Note>(this.AddDescription, AddDes)
  }
  EditDescription_Code_Note(note_id: number, EditDes: Note) {
    return this.http.put<Note>(this.EditDescription + note_id, EditDes)
  }
}
