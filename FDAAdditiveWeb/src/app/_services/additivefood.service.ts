import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class AdditiveFoodService {


  private baseUrl = 'http://localhost:5000/api/v1/additivefoods/foodcode';
  private baseUrlAdditiveID = 'http://localhost:5000/api/v1/additivefoods';
  private baseUrlAdditiveFoodByFoodID = 'http://localhost:5000/api/v1/additivefoods/id';


  constructor(private http: HttpClient) { }

  GetAdditiveFoodByFoodID(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlAdditiveFoodByFoodID}/${id}`);
  }


  GetAdditiveFoodByFoodCode(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  GetAdditiveFood(id: number): Observable<any> {
    return this.http.get(`${this.baseUrlAdditiveID}/${id}`);
  }
}
