import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdditiveSynonym } from '../_models/additivesynonym';

@Injectable({ providedIn: 'root' })

export class AdditiveSynonymService {

  private baseUrlGetAll = 'http://localhost:5000/api/v1/additivesynonym';
  private baseUrl = 'http://localhost:5000/api/v1/additivesynonym';
  private baseUrlAdditiveSynonym = 'http://localhost:5000/api/v1/additivesynonym/details';
  private baseUrlAddSynonym = 'http://localhost:5000/api/v1/additivesynonym/api/v1/Synonym/create';



  constructor(private http: HttpClient) { }


  GetAllSynonym() {
    return this.http.get<AdditiveSynonym>(this.baseUrlGetAll);
  }


  GetAdditiveSynonymData(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  GetAdditiveSynonymByAdditiveSynonyms(id: string): Observable<any> {
    return this.http.get(`${this.baseUrlAdditiveSynonym}/${id}`);
  }


  AddSynonymData(AddSynonym: AdditiveSynonyms) {
    return this.http.post<AdditiveSynonyms>(this.baseUrlAddSynonym, AddSynonym)
  }

}
interface AdditiveSynonyms {
  additives_id: number;
  //ins_id: string;
  //additives_description: string;
  additives_synonyms: string;
  //groups_description: string;
}
