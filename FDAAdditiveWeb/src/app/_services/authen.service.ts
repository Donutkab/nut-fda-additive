import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenService {

  constructor() { }
  private accessKey = 'Key_accessToken';

  //กำหนด access token ไว้ในความจำ browser
  setAuthentication(accessToken: string): void {
    localStorage.setItem(this.accessKey, accessToken);
  }

  //ดีงค่า access token ไว้ในความจำ browser ออกมา
  getAuthentication(): string {
    return localStorage.getItem(this.accessKey);
  }
  //ล้างค่า access token ที่อยู่ในความจำ browser
  clearAuthentication(): void {
    localStorage.removeItem(this.accessKey);
  }
}
