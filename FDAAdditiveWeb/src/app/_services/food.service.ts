import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Food } from '../_models/food';
import { Observable } from 'rxjs';
import { AdditiveFood } from '../_models/additivefood';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  private baseUrlAddAdditive = 'http://localhost:5000/api/v1/additives/api/v1/additive/create';
  private baseUrlFoodParentIdFirst = 'http://localhost:5000/api/v1/Food/foods/foodslist';
  private baseUrlFoodParentIdForDropdown = 'http://localhost:5000/api/v1/Food/foods';
  private baseUrlGetFoodById = 'http://localhost:5000/api/v1/Food';
  //private baseUrlGetFoodById = 'http://localhost:5000/api/v1/Food';
  private baseUrlAddFood = 'http://localhost:5000/api/v1/Food/api/v1/food/create';
  private baseUrlGetByFoodName = 'http://localhost:5000/api/v1/Food/foodsname';
  private baseUrlUpdateFood= ' http://localhost:5000/api/v1/Food/api/v1/Food/update?id=';

 


  //private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  //headers = new HttpHeaders({ "content-type": "application/json; charset=UTF-8", "Accept": "application/json" });



  //head = new Headers({ 'Content-Type': 'application/json' });

  //headers = new HttpHeaders({
  //   'Content-Type': 'application/json'
  // });
  //options = { headers: this.headers };

  //httpOptions = {
  //  headers: new HttpHeaders({ 'Content-Type': 'text/plain' })
  //}

  constructor(private http: HttpClient) { }

  GetFoodParentIdList() {
    return this.http.get<Food[]>(this.baseUrlFoodParentIdFirst);
  }

  GetFoodById(foods_id: number) {
    return this.http.get<Food>(this.baseUrlGetFoodById + "/" + foods_id);
  }

  GetAllFoodByParentIdForDropdown(foods_id: number) {
    return this.http.get<Food>(this.baseUrlFoodParentIdForDropdown + "/" + foods_id);
  }

  AddFood(food: FoodDropdown) {
    return this.http.post<FoodDropdown[]>(this.baseUrlAddFood, food);
  }


  GetDataByFoodName(foods_id: number) {
    return this.http.get<AdditiveFood[]>(this.baseUrlGetByFoodName + "/" + foods_id);
  }

  //GetDataByFoodName(ins: string): Observable<any> {
  //  return this.http.get<AdditiveFood>(`${this.baseUrlGetByFoodName}/${ins}`);
  //}
}



interface FoodDropdown {

  foods_code: string;
  foods_name: string;
  foods_description_th: string;
  foods_description_en: string;
  parent_id: number;
  parent_id_1: number;
  parent_id_2: number;
  parent_id_3: number;
}
