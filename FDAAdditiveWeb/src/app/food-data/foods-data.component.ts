import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-foods-data',
  templateUrl: './foods-data.component.html',
  styleUrls: ['./foods-data.component.css']
})
export class FoodsDataComponent implements OnInit {
  public foods: Foods[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Foods[]>('http://localhost:5000/api/v1/Food').subscribe(result => {
      this.foods = result;
    }, error => console.error(error));
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
}

interface Foods {
  foods_id: number;
  foods_code: string;
  foods_name: string;
  foods_description_th: string;
  foods_description_en: string;
}
