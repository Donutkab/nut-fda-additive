import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-notes-data',
  templateUrl: './notes-data.component.html',
  styleUrls: ['./notes-data.component.css']
})
export class NotesDataComponent implements OnInit {
  public notes: Notes[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Notes[]>('http://localhost:5000/api/v1/notes').subscribe(result => {
      this.notes = result;
    }, error => console.error(error));
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
}

interface Notes {
  notes_id: number;
  notes_description: string;
}
