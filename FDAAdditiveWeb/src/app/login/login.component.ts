/// <reference path="../backend/notes/notes.component.ts" />
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { error } from 'protractor';
import { ILoginComponent } from './login.interface';
import { AlertService } from '../_services/alert.service';
import { AuthURL } from '../authentication/authentication.url';
import { AccountService } from '../shareds/services/account.service';
import { AuthenService } from '../_services/authen.service';
import { User } from '../_models';
import { UserService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  add: User;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {

  }


  ClickAddDes(f: NgForm) {
    //alert("ok")
    this.add = f.value
    alert(JSON.stringify(this.add))
    console.log(this.add);
    this.userService.UserLogin(this.add).subscribe(
      data => {
        this.username = data.username;
        this.password = data.password;
        //alert(JSON.stringify(data))
        //console.log(data);
        if (data.count>0) {

          //alert(data.count);
          this.router.navigate(['/backend'], { queryParams: { textSearch: data } })

        }
      });




    //this.noteservice.AddDescription_Code_Note(this.add).subscribe(data => {
    //  window.location.reload();
    //  this.notes_id = data.notes_id;
    //  this.notes_code = data.notes_code;
    //  this.notes_description = data.notes_description;
    //});
  }



  //สร้างฟอร์ม
  //  private initialCreateFormData() {
  //    this.form = this.builder.group({
  //      email: ['', Validators.required],
  //      password: ['', Validators.required],
  //      remember: [true]
  //    });
  //  }

  //}
}



export const AppURL = {
  Login: 'login',
  Register: 'register',
  Authen: 'authentication'
};
