import { FormGroup } from '@angular/forms';

export interface ILoginComponent {
  form: FormGroup;
  Url: any;
  onSubmit(): void;

}


export interface ILogin {
  email: string;
  password: string;
  remember: boolean;
}
