import { Component, OnInit } from '@angular/core';
import { AdditiveFood } from '../_models/additivefood';
import { ActivatedRoute } from '@angular/router';
import { AdditiveFoodService } from '../_services/additivefood.service';
import { AdditiveService } from '../_services/additive.service';
import { FoodService } from '../_services/food.service';
import { Food } from '../_models/food';

@Component({
  selector: 'app-additive-food-search-details',
  templateUrl: './additive-food-search-details.component.html',
  styleUrls: ['./additive-food-search-details.component.css']
})
export class AdditiveFoodSearchDetailsComponent implements OnInit {


  additiveFoodInfo: AdditiveFood[];
  foodnamedatas: AdditiveFood[];
  fooddata: AdditiveFood[];
  additivedataByFoodSearch: AdditiveFood[];

  datainformation: number[];
  fooddatabyFoodId: AdditiveFood;

  constructor(private route: ActivatedRoute, private additiveFoodService: AdditiveFoodService, private additiveService: AdditiveService, private foodService: FoodService) { }

  dtOptions: DataTables.Settings = {};
  text: number;
  value: number;
  datatest: Food

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    //this.text = this.route.snapshot.queryParams['id']
    let value = this.route.snapshot.params['id']
    let texts = this.route.snapshot.queryParams;
    let doc = {
      foods_id: texts.foods_id,
      foods_code: texts.foods_code,
        foods_name: texts.foods_name,
        foods_description_th: texts.foods_description_th,
        foods_description_en: texts.foods_description_en,
        parent_id: undefined
    }

    this.datatest = doc;
    alert(this.datatest.foods_name);
    console.log(this.datatest);

    this.additiveFoodService.GetAdditiveFoodByFoodID(value).subscribe(
      data => {
        this.fooddatabyFoodId = data;



       
        //console.log(data)
        //alert(this.foodnamedatas);
        this.fooddata = data;
        //alert(JSON.stringify(this.fooddata));
        //console.log(this.fooddata)

        //alert(this.fooddata[0].foods_id);
        //alert(JSON.stringify(this.foodnamedatas));
        //alert(JSON.stringify(this.foodnamedatas.foods_id));
        //console.log(this.foodnamedatas[0].foods_id)


         
        let AFD = [];
        AFD = data;
        //for (let i = 0; i < AFD.length;i++) {
        //  console.log(i)
        //}

        AFD.forEach(async (item, i) => {
          //console.log(item.foods_id)
          await this.additiveFoodService.GetAdditiveFoodByFoodID(item.foods_id).subscribe(
            data => {
              //console.log(i);
              this.additivedataByFoodSearch = data
              //console.log(this.additivedataByFoodSearch);
            }
          )
        })





      }
    )




  }


  async test(foods_id: number) {
    alert(foods_id);

    console.log(this.additivedataByFoodSearch);

    //this.datainformation = foods_id;
    //await this.additiveFoodService.GetAdditiveFoodByFoodID(foods_id).subscribe(
    //  async data => {

    //    this.additivedataByFoodSearch = data;
    //    console.log(this.additivedataByFoodSearch);
    //  }


    //  }
    //}
    //)


    //this.additiveFoodService.GetAdditiveFoodByFoodCode(this.value)
    //  .subscribe(data => {
    //    this.additiveFoodInfo = data;
    //    //console.log(data);
    //  });

  }
}
