import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditiveFoodSearchDetailsComponent } from './additive-food-search-details.component';

describe('AdditiveFoodSearchDetailsComponent', () => {
  let component: AdditiveFoodSearchDetailsComponent;
  let fixture: ComponentFixture<AdditiveFoodSearchDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditiveFoodSearchDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditiveFoodSearchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
