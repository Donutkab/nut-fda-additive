import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdditiveFoodService } from '../_services/additivefood.service';
import { AdditiveService } from '../_services/additive.service';
import { Additive } from '../_models/additive';
import { AdditiveFood } from '../_models/additivefood';
import { FoodService } from '../_services/food.service';
import { Food } from '../_models/food';

@Component({
  selector: 'app-additive-food-search',
  templateUrl: './additive-food-search.component.html',
  styleUrls: ['./additive-food-search.component.css']
})
export class AdditiveFoodSearchComponent implements OnInit {

  additiveFoodInfo: AdditiveFood[];
  foodnamedatas: AdditiveFood[];
  fooddata: AdditiveFood[];
  additivedataByFoodSearch: AdditiveFood[];

  datainformation: number[];

  constructor(private route: ActivatedRoute, private additiveFoodService: AdditiveFoodService, private additiveService: AdditiveService, private foodService: FoodService) { }

  dtOptions: DataTables.Settings = {};
  text: string;
  value: number;


  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    let value = this.route.snapshot.queryParams['textSearch']
    this.text = value;
    //alert(this.text);
    //console.log(this.text);

    this.foodService.GetDataByFoodName(value).subscribe(
      data => {

        this.foodnamedatas = data;
        this.fooddata = data;
        //alert(JSON.stringify(this.fooddata));
        //console.log(this.foodnamedatas)

        //alert(this.fooddata[0].foods_id);
        //alert(JSON.stringify(this.foodnamedatas));
        //alert(JSON.stringify(this.foodnamedatas.foods_id));
        //console.log(this.foodnamedatas[0].foods_id)


        let AFD = [];
        AFD = data;
        //for (let i = 0; i < AFD.length;i++) {
        //  console.log(i)
        //}

        AFD.forEach(async (item, i) => {
          //console.log(item.foods_id)
          await this.additiveFoodService.GetAdditiveFoodByFoodID(item.foods_id).subscribe(
            data => {
              //console.log(i);
              this.additivedataByFoodSearch = data
              //console.log(this.additivedataByFoodSearch);
            }
          )
        })





      }
    )




  }


  async test(foods_id: number) {
    alert(foods_id);

    console.log(this.additivedataByFoodSearch);

    //this.datainformation = foods_id;
    //await this.additiveFoodService.GetAdditiveFoodByFoodID(foods_id).subscribe(
    //  async data => {
       
    //    this.additivedataByFoodSearch = data;
    //    console.log(this.additivedataByFoodSearch);
    //  }


      //  }
      //}
    //)


    //this.additiveFoodService.GetAdditiveFoodByFoodCode(this.value)
    //  .subscribe(data => {
    //    this.additiveFoodInfo = data;
    //    //console.log(data);
    //  });

  }
}
