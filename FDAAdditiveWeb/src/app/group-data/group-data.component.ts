import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-group-data',
  templateUrl: './group-data.component.html',
  styleUrls: ['./group-data.component.css']
})
export class GroupDataComponent {
  public groups: Groups[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Groups[]>('http://localhost:5000/api/v1/groups').subscribe(result => {
      this.groups = result;
    }, error => console.error(error));
  }
  dtOptions: DataTables.Settings = {};
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
}

interface Groups {
  groups_id: number;
  groups_description: string;
}
