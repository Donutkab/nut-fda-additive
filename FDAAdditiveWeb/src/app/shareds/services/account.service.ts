import { Injectable } from '@angular/core';
//import { IRegister } from '../../component/register/register.interface';
import { resolve } from 'url';
//import { ILoginComponent, ILogin } from '../../component/login/login.interface';
import { promise } from 'protractor';
import { Data } from '@angular/router';
import { ILogin } from '../../login/login.interface';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor() { }
  private mockuserItem: IAccount[] = [
    {
      id: 1,
      firstname: 'Nut',
      lastname: 'Lee',
      email: 'nut_lee@hotmail.com',
      password: '111111',
      position: 'FrontEnd Developer',
      image: 'https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg',
      create: new Date(),
      update: new Date()
    },
    {
      id: 2,
      firstname: 'Nut2',
      lastname: 'Lee2',
      email: 'nut2_lee@hotmail.com',
      password: '111111',
      position: 'Backend Developer',
      image: null,
      create: new Date(),
      update: new Date()
    }
  ];

  //ดึงข้อมูลผู้ที่เข้าสู่ระบบจาก Token
  getUserLogin(accessToken: string) {
    return new Promise<IAccount>((resolve, reject) => {
      const userLogin = this.mockuserItem.find(m => m.id == accessToken);
      if (!userLogin) return reject({ Message: 'accessToken ไม่ถูกต้อง' });
      resolve(userLogin);
    });
  }

  //เข้าสู่ระบบ
  onLogin(model: ILogin) {
    return new Promise<{ accessToken: string }>((resolve, reject) => {
      //resolve(model)
      const userLogin = this.mockuserItem.find(item => item.email == model.email && item.password == model.password);
      if (!userLogin) return reject({ Message: 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง' });
      //resolve(userLogin);
      resolve({
        accessToken: userLogin.id
      });
    });
  }

  //ลงทะเบียน
  //onRegister(model: IRegister) {
  //  //console.log(model);
  //  return new Promise((resolve, reject) => {
  //    model['id'] = Math.random();
  //    this.mockuserItem.push(model);
  //    resolve(model)

  //    //reject({
  //    //  Message: 'Error from server!'});
  //  });
  //}
}


export interface IAccount {
  firstname: string;
  lastname: string;
  email: string;
  password: string;


  id?: any;
  position?: string;
  image?: string;
  create?: Date;
  update?: Date;
}


