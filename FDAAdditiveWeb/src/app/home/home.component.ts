import { Component, OnInit, Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Functional } from '../_models/functional';
import { FunctionalService } from '../_services/functional.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Additive } from '../_models/additive';
import { NgForm } from '@angular/forms';
import { AdditiveService } from '../_services/additive.service';
import { AdditiveSynonymService } from '../_services/additivesynonym.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent{
  functionals: Functional;
  additives: Additive;
  INSSearch: string = '';
  AdditiveSearch: string = '';
  food_category_code: string ='';

  constructor(private route: ActivatedRoute, private router: Router, private functionalService: FunctionalService, private additiveService: AdditiveService, private additiveSynonymService: AdditiveSynonymService) { }





  ngOnInit() {
    this.functionalService.GetAllFunctional()
      .subscribe(data => {
        this.functionals = data;
      }, error => console.log(error));

  }


  onSubmitINS(f: NgForm) {
    this.additives = f.value;
    let value = f.value.INSSearch
    this.router.navigate(['/additive/detail'], { queryParams: { id: value } })
  }


  onSubmitAdditive(Additive: NgForm) {
    this.additives = Additive.value;
    let value = Additive.value.AdditiveSearch
    this.router.navigate(['/additive-groupname-synonym'], { queryParams: { textSearch: value } })
  }

  onSubmitFoodSearch(FoodSearch: NgForm) {
    this.additives = FoodSearch.value;
    let value = FoodSearch.value.food_category_code
    this.router.navigate(['/additive-food-search'], { queryParams: { textSearch: value } })
  }

}
