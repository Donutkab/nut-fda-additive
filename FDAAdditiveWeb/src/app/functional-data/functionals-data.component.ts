import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-functionals-data',
  templateUrl: './functionals-data.component.html',
  styleUrls: ['./functionals-data.component.css']
})
export class FunctionalsDataComponent implements OnInit {
  public functionals: Functionals[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Functionals[]>('http://localhost:5000/api/v1/Functional').subscribe(result => {
      this.functionals = result;
    }, error => console.error(error));
  }
  dtOptions: DataTables.Settings = {};
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }

 

 
}

interface Functionals {
  functional_id: number;
  functional_description: string;
}
