import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { AdditiveDataComponent } from './additive-data/additive-data.component';
import { NotesDataComponent } from './note-data/notes-data.component';
import { FoodsDataComponent } from './food-data/foods-data.component';
import { FunctionalsDataComponent } from './functional-data/functionals-data.component';
import { GroupDataComponent } from './group-data/group-data.component';
import { IndexComponent } from './backend/index/index.component';

import { DataTablesModule } from 'angular-datatables';
import { AdditiveDetailComponent } from './additive-detail/additive-detail.component';
import { FunctionalDetailComponent } from './functional-detail/functional-detail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';

import { AdditiveGroupnameSynonymComponent } from './additive-groupname-synonym/additive-groupname-synonym.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { BackendAdditiveComponent } from './backend/additive/additive.component';
import { BackendFoodComponent } from './backend/food/food.component';
import { BackendAdditiveDetailComponent } from './backend/additive-detail/additive-detail.component';
import { BackendFunctionalsComponent } from './backend/functionals/functionals.component';
import { BackendGroupsComponent } from './backend/groups/groups.component';
import { BackendNotesComponent } from './backend/notes/notes.component';
import { AdditiveFoodSearchComponent} from './additive-food-search/additive-food-search.component';
import { AddfunctionalDetailComponent } from './backend/addfunctional-detail/addfunctional-detail.component';
import { AddnoteComponent } from './backend/addnote/addnote.component';
import { AddsynonymComponent } from './backend/addsynonym/addsynonym.component';
import { AdditiveFoodSearchDetailsComponent } from './additive-food-search-details/additive-food-search-details.component';




@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    AdditiveDataComponent,
    NotesDataComponent,
    FoodsDataComponent,
    FunctionalsDataComponent,
    GroupDataComponent,
    AdditiveDetailComponent,
    FunctionalDetailComponent,
    LoginComponent,
    AdditiveGroupnameSynonymComponent,
    IndexComponent,
    GroupDetailComponent,
    BackendAdditiveComponent,
    BackendFoodComponent,
    BackendAdditiveDetailComponent,
    BackendFunctionalsComponent,
    BackendGroupsComponent,
    BackendNotesComponent,
    AdditiveFoodSearchComponent,
    AddfunctionalDetailComponent,
    AddnoteComponent,
    AddsynonymComponent,
    AdditiveFoodSearchDetailsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'additives', component: AdditiveDataComponent },
      { path: 'notes', component: NotesDataComponent },
      { path: 'foods', component: FoodsDataComponent },
      { path: 'functionals', component: FunctionalsDataComponent },
      { path: 'groups', component: GroupDataComponent },
      { path: 'additive/detail/:id', component: AdditiveDetailComponent },
      { path: 'functional/detail/:id', component: FunctionalDetailComponent },
      { path: 'functional/detail/:id', component: GroupDetailComponent },
      { path: 'login', component: LoginComponent },
      { path: 'additive/detail', component: AdditiveDetailComponent },
      { path: 'additive-groupname-synonym', component: AdditiveGroupnameSynonymComponent },
      { path: 'additive-groupname-synonym/:id', component: AdditiveGroupnameSynonymComponent },
      { path: 'backend', component: IndexComponent },
      { path: 'backend/additives', component: BackendAdditiveComponent },
      { path: 'backend/foods', component: BackendFoodComponent },
      { path: 'backend/additive/detail/:id', component: BackendAdditiveDetailComponent },
      { path: 'backend/functionals', component: BackendFunctionalsComponent },
      { path: 'backend/groups', component: BackendGroupsComponent },
      { path: 'backend/notes', component: BackendNotesComponent },
      { path: 'additive-food-search', component: AdditiveFoodSearchComponent },
      { path: 'backend/addfunctional-detail/:id', component: AddfunctionalDetailComponent },
      { path: 'backend/addnotes', component: AddnoteComponent },
      { path: 'backend/addsynonym', component: AddsynonymComponent },
      { path: 'additive-food-search-details/detail/:id', component: AdditiveFoodSearchDetailsComponent },
      
    ]),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
