import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Functional } from '../_models/functional';
import { FunctionalService } from '../_services/functional.service';

@Component({
  selector: 'app-functional-detail',
  templateUrl: './functional-detail.component.html',
  styleUrls: ['./functional-detail.component.css']
})
export class FunctionalDetailComponent implements OnInit{

  id: number;
  functionals: Functional;
  functionalids: Functional;

  constructor(private route: ActivatedRoute, private router: Router, private functionalService: FunctionalService) { }

  ngOnInit() {
    this.functionals = new Functional();

    this.id = this.route.snapshot.params['id'];

    this.functionalService.GetAdditiveFunctionalsByFunctionalID(this.id)
      .subscribe(data => {
        console.log(data)
        this.functionals = data;
      }, error => console.log(error));

    this.functionalids = new Functional();
    this.id = this.route.snapshot.params['id'];

    this.functionalService.GetFunctionalDataByID(this.id)
      .subscribe(data => {
        console.log(data)
        this.functionalids = data;
      }, error => console.log(error));

  

   
  }
}
